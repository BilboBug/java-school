package ru.javaschool.lesson18.example1;

import ru.javaschool.lesson18.example1.chassis.CarChassis;
import ru.javaschool.lesson18.example1.chassis.TruckChassis;
import ru.javaschool.lesson18.example1.chassis.VanChassis;
import ru.javaschool.lesson18.example1.engine.CarEngine;
import ru.javaschool.lesson18.example1.engine.TruckEngine;
import ru.javaschool.lesson18.example1.engine.VanEngine;
import ru.javaschool.lesson18.example1.vehicle.Car;
import ru.javaschool.lesson18.example1.vehicle.MotorVehicle;
import ru.javaschool.lesson18.example1.vehicle.Truck;
import ru.javaschool.lesson18.example1.vehicle.Van;
import ru.javaschool.lesson18.example1.wheels.CarWheels;
import ru.javaschool.lesson18.example1.wheels.TruckWheels;
import ru.javaschool.lesson18.example1.wheels.VanWheels;

public class Main {

    public static void main(String[] args) {
        CarChassis carChassis = new CarChassis();
        CarEngine carEngine = new CarEngine();
        CarWheels carWheels = new CarWheels();
        MotorVehicle car = new Car(carChassis, carEngine, carWheels);
        System.out.println(car);

        VanChassis vanChassis = new VanChassis();
        VanEngine vanEngine = new VanEngine();
        VanWheels vanWheels = new VanWheels();
        MotorVehicle van = new Van(vanChassis, vanEngine, vanWheels);
        System.out.println(van);

        TruckChassis truckChassis = new TruckChassis();
        TruckEngine truckEngine = new TruckEngine();
        TruckWheels truckWheels = new TruckWheels();
        MotorVehicle truck = new Truck(truckChassis, truckEngine, truckWheels);
        System.out.println(truck);
    }


}
