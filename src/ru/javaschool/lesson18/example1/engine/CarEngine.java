package ru.javaschool.lesson18.example1.engine;

public class CarEngine implements Engine {

    private String parts;

    @Override
    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
