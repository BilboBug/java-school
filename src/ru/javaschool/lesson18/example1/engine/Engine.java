package ru.javaschool.lesson18.example1.engine;

public interface Engine {
    String getEngineParts();
}
