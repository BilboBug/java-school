package ru.javaschool.lesson18.example1.engine;

public class VanEngine implements Engine {

    private String parts;

    @Override
    public String getEngineParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanEngine{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
