package ru.javaschool.lesson18.example1.wheels;

public class CarWheels implements Wheels {

    private String parts;

    @Override
    public String getWheelsParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarWheels{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
