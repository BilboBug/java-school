package ru.javaschool.lesson18.example1.wheels;

public interface Wheels {
    String getWheelsParts();
}
