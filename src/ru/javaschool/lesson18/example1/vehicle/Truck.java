package ru.javaschool.lesson18.example1.vehicle;

import ru.javaschool.lesson18.example1.chassis.TruckChassis;
import ru.javaschool.lesson18.example1.engine.TruckEngine;
import ru.javaschool.lesson18.example1.wheels.TruckWheels;

public class Truck extends MotorVehicle {

    public Truck(TruckChassis chassis, TruckEngine engine, TruckWheels wheels) {
        super(chassis, engine, wheels);
    }

    @Override
    public String toString() {
        return "Truck{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                '}';
    }
}
