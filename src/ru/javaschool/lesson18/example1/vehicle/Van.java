package ru.javaschool.lesson18.example1.vehicle;

import ru.javaschool.lesson18.example1.chassis.VanChassis;
import ru.javaschool.lesson18.example1.engine.VanEngine;
import ru.javaschool.lesson18.example1.wheels.VanWheels;

public class Van extends MotorVehicle {

    public Van(VanChassis chassis, VanEngine engine, VanWheels wheels) {
        super(chassis, engine, wheels);
    }

    @Override
    public String toString() {
        return "Van{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                '}';
    }
}
