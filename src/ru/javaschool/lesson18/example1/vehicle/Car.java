package ru.javaschool.lesson18.example1.vehicle;

import ru.javaschool.lesson18.example1.chassis.CarChassis;
import ru.javaschool.lesson18.example1.engine.CarEngine;
import ru.javaschool.lesson18.example1.wheels.CarWheels;

public class Car extends MotorVehicle {

    public Car(CarChassis chassis, CarEngine engine, CarWheels wheels) {
        super(chassis, engine, wheels);
    }

    @Override
    public String toString() {
        return "Car{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                '}';
    }
}
