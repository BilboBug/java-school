package ru.javaschool.lesson18.example1.vehicle;

import ru.javaschool.lesson18.example1.chassis.Chassis;
import ru.javaschool.lesson18.example1.engine.Engine;
import ru.javaschool.lesson18.example1.wheels.Wheels;

public abstract class MotorVehicle {

    private Chassis chassis;
    private Engine engine;
    private Wheels wheels;

    public MotorVehicle(Chassis chassis, Engine engine, Wheels wheels) {
        this.chassis = chassis;
        this.engine = engine;
        this.wheels = wheels;
    }

    public Chassis getChassis() {
        return chassis;
    }

    public Engine getEngine() {
        return engine;
    }

    public Wheels getWheels() {
        return wheels;
    }
}
