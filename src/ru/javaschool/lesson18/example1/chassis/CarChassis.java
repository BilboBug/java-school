package ru.javaschool.lesson18.example1.chassis;

public class CarChassis implements Chassis {

    private String parts;

    @Override
    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "CarChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
