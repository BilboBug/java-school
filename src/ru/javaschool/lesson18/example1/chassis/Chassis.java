package ru.javaschool.lesson18.example1.chassis;

public interface Chassis {
    String getChassisParts();
}
