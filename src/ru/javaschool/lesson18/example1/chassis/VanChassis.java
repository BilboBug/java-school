package ru.javaschool.lesson18.example1.chassis;

public class VanChassis implements Chassis {

    private String parts;

    @Override
    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "VanChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
