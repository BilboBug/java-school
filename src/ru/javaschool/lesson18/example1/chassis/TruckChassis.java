package ru.javaschool.lesson18.example1.chassis;

public class TruckChassis implements Chassis {

    private String parts;

    @Override
    public String getChassisParts() {
        return parts;
    }

    @Override
    public String toString() {
        return "TruckChassis{" +
                "parts='" + parts + '\'' +
                '}';
    }
}
