package ru.javaschool.lesson18.example3.vehicle;

import ru.javaschool.lesson18.example3.logisticsystem.logistic.AircraftLogistic;
import ru.javaschool.lesson18.example3.logisticsystem.logistic.Logistic;

public class Aircraft extends Vehicle {

    @Override
    public void go() {

    }

    @Override
    public Logistic createLogistic() {
        return new AircraftLogistic();
    }
}
