package ru.javaschool.lesson18.example3.vehicle;

import ru.javaschool.lesson18.example3.logisticsystem.Route;
import ru.javaschool.lesson18.example3.logisticsystem.LogisticCreator;

public abstract class Vehicle implements LogisticCreator {

    private Route route;

    public Vehicle() {
    }

    public abstract void go();

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }
}
