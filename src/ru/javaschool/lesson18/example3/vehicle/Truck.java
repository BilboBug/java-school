package ru.javaschool.lesson18.example3.vehicle;

import ru.javaschool.lesson18.example3.logisticsystem.logistic.Logistic;
import ru.javaschool.lesson18.example3.logisticsystem.logistic.TruckLogistic;

public class Truck extends Vehicle {

    public Truck() {
    }

    public void go() {
        // some implementation
    }

    @Override
    public Logistic createLogistic() {
        return new TruckLogistic();
    }
}
