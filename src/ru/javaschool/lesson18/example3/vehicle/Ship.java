package ru.javaschool.lesson18.example3.vehicle;

import ru.javaschool.lesson18.example3.logisticsystem.logistic.Logistic;
import ru.javaschool.lesson18.example3.logisticsystem.logistic.ShipLogistic;

public class Ship extends Vehicle {

    @Override
    public void go() {
    }

    @Override
    public Logistic createLogistic() {
        return new ShipLogistic();
    }
}
