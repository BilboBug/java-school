package ru.javaschool.lesson18.example3.logisticsystem;

import ru.javaschool.lesson18.example3.logisticsystem.logistic.Logistic;

public interface LogisticCreator {
    Logistic createLogistic();
}
