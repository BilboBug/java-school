package ru.javaschool.lesson18.example3.logisticsystem;

import ru.javaschool.lesson18.example3.vehicle.Vehicle;

public class LogisticSystem {

    public Route createRoute(Vehicle vehicle, Point departure, Point destination) {
        return vehicle.createLogistic().createRout(departure, destination);
    }
}
