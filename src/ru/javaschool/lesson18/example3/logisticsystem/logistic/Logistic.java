package ru.javaschool.lesson18.example3.logisticsystem.logistic;

import ru.javaschool.lesson18.example3.logisticsystem.Point;
import ru.javaschool.lesson18.example3.logisticsystem.Route;

public interface Logistic {
    Route createRout(Point departure, Point destination);
}
