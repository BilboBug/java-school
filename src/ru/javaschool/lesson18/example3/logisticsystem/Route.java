package ru.javaschool.lesson18.example3.logisticsystem;

import java.util.List;

public class Route {

    private List<Point> points;

    public Route(List<Point> points) {
        this.points = points;
    }

    public List<Point> getPoints() {
        return points;
    }
}
