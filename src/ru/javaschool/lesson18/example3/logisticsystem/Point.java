package ru.javaschool.lesson18.example3.logisticsystem;

public class Point {

    private String name;


    public Point(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
