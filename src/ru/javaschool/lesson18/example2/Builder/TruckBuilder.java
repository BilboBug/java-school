package ru.javaschool.lesson18.example2.Builder;

import ru.javaschool.lesson18.example2.brakes.ABS;
import ru.javaschool.lesson18.example2.chassis.TruckChassis;
import ru.javaschool.lesson18.example2.climate.AirConditioning;
import ru.javaschool.lesson18.example2.climate.ClimateControlSystem;
import ru.javaschool.lesson18.example2.climate.SeatHeating;
import ru.javaschool.lesson18.example2.engine.TruckEngine;
import ru.javaschool.lesson18.example2.steering.Steering;
import ru.javaschool.lesson18.example2.vehicle.Truck;
import ru.javaschool.lesson18.example2.vehicle.Vehicle;
import ru.javaschool.lesson18.example2.wheels.TruckWheels;

public class TruckBuilder implements VehicleBuilder {
    private TruckChassis truckChassis;
    private TruckEngine truckEngine;
    private TruckWheels truckWheels;
    private ABS abs;
    private AirConditioning airConditioning;
    private ClimateControlSystem climateControlSystem;
    private SeatHeating seatHeating;
    private Steering steering;

    @Override
    public void buildChassis() {
        truckChassis = new TruckChassis();
    }

    @Override
    public void buildEngine() {
        truckEngine = new TruckEngine();
    }

    @Override
    public void buildWheels() {
        truckWheels = new TruckWheels();
    }

    @Override
    public void buildABS() {
        abs = new ABS();
    }

    @Override
    public void buildAirConditioning() {
        airConditioning = new AirConditioning();
    }

    @Override
    public void buildClimateControlSystem() {
        climateControlSystem = new ClimateControlSystem();
    }

    @Override
    public void buildSeatHeating() {
        seatHeating = new SeatHeating();
    }

    @Override
    public void buildSteering(Steering steering) {
        this.steering = steering;
    }

    @Override
    public Vehicle getVehicle() {
        return new Truck(truckChassis, truckEngine, truckWheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }
}
