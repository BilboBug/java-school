package ru.javaschool.lesson18.example2.Builder;

import ru.javaschool.lesson18.example2.steering.StandardSteering;
import ru.javaschool.lesson18.example2.vehicle.Vehicle;

public class MinimumEquipmentDirector implements VehicleDirector {

    @Override
    public Vehicle build(VehicleBuilder builder) {
        builder.buildChassis();
        builder.buildEngine();
        builder.buildWheels();
        builder.buildSteering(new StandardSteering());
        return builder.getVehicle();
    }
}
