package ru.javaschool.lesson18.example2.Builder;

import ru.javaschool.lesson18.example2.vehicle.Vehicle;

public interface VehicleDirector {
    Vehicle build(VehicleBuilder builder);
}
