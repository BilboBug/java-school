package ru.javaschool.lesson18.example2.Builder;

import ru.javaschool.lesson18.example2.steering.PowerSteering;
import ru.javaschool.lesson18.example2.vehicle.Vehicle;

public class MaximumEquipmentDirector implements VehicleDirector {
    @Override
    public Vehicle build(VehicleBuilder builder) {
        builder.buildChassis();
        builder.buildEngine();
        builder.buildWheels();
        builder.buildSteering(new PowerSteering());
        builder.buildClimateControlSystem();
        builder.buildAirConditioning();
        builder.buildSeatHeating();
        builder.buildABS();
        return builder.getVehicle();
    }
}
