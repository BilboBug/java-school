package ru.javaschool.lesson18.example2.Builder;

import ru.javaschool.lesson18.example2.steering.Steering;
import ru.javaschool.lesson18.example2.vehicle.Vehicle;

public interface VehicleBuilder {
    void buildChassis();

    void buildEngine();

    void buildWheels();

    void buildABS();

    void buildAirConditioning();

    void buildClimateControlSystem();

    void buildSeatHeating();

    void buildSteering(Steering steering);

    Vehicle getVehicle();
}
