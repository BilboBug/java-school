package ru.javaschool.lesson18.example2.Builder;

import ru.javaschool.lesson18.example2.brakes.ABS;
import ru.javaschool.lesson18.example2.chassis.CarChassis;
import ru.javaschool.lesson18.example2.climate.AirConditioning;
import ru.javaschool.lesson18.example2.climate.ClimateControlSystem;
import ru.javaschool.lesson18.example2.climate.SeatHeating;
import ru.javaschool.lesson18.example2.engine.CarEngine;
import ru.javaschool.lesson18.example2.steering.Steering;
import ru.javaschool.lesson18.example2.vehicle.Car;
import ru.javaschool.lesson18.example2.vehicle.Vehicle;
import ru.javaschool.lesson18.example2.wheels.CarWheels;

public class CarBuilder implements VehicleBuilder {
    private ABS abs;
    private CarChassis carChassis;
    private AirConditioning airConditioning;
    private ClimateControlSystem climateControlSystem;
    private SeatHeating seatHeating;
    private CarEngine carEngine;
    private Steering steering;
    private CarWheels carWheels;

    @Override
    public void buildChassis() {
        carChassis = new CarChassis();
    }

    @Override
    public void buildEngine() {
        carEngine = new CarEngine();
    }

    @Override
    public void buildWheels() {
        carWheels = new CarWheels();
    }

    @Override
    public void buildABS() {
        abs = new ABS();
    }

    @Override
    public void buildAirConditioning() {
        airConditioning = new AirConditioning();
    }

    @Override
    public void buildClimateControlSystem() {
        climateControlSystem = new ClimateControlSystem();
    }

    @Override
    public void buildSeatHeating() {
        seatHeating = new SeatHeating();
    }

    @Override
    public void buildSteering(Steering steering) {
        this.steering = steering;
    }

    @Override
    public Vehicle getVehicle() {
        return new Car(carChassis, carEngine, carWheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }
}
