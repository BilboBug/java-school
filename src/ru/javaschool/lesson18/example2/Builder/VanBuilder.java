package ru.javaschool.lesson18.example2.Builder;

import ru.javaschool.lesson18.example2.brakes.ABS;
import ru.javaschool.lesson18.example2.chassis.VanChassis;
import ru.javaschool.lesson18.example2.climate.AirConditioning;
import ru.javaschool.lesson18.example2.climate.ClimateControlSystem;
import ru.javaschool.lesson18.example2.climate.SeatHeating;
import ru.javaschool.lesson18.example2.engine.VanEngine;
import ru.javaschool.lesson18.example2.steering.Steering;
import ru.javaschool.lesson18.example2.vehicle.Van;
import ru.javaschool.lesson18.example2.vehicle.Vehicle;
import ru.javaschool.lesson18.example2.wheels.VanWheels;

public class VanBuilder implements VehicleBuilder {
    private VanChassis vanChassis;
    private VanEngine vanEngine;
    private VanWheels vanWheels;
    private ABS abs;
    private AirConditioning airConditioning;
    private ClimateControlSystem climateControlSystem;
    private SeatHeating seatHeating;
    private Steering steering;

    @Override
    public void buildChassis() {
        vanChassis = new VanChassis();
    }

    @Override
    public void buildEngine() {
        vanEngine = new VanEngine();
    }

    @Override
    public void buildWheels() {
        vanWheels = new VanWheels();
    }

    @Override
    public void buildABS() {
        abs = new ABS();
    }

    @Override
    public void buildAirConditioning() {
        airConditioning = new AirConditioning();
    }

    @Override
    public void buildClimateControlSystem() {
        climateControlSystem = new ClimateControlSystem();
    }

    @Override
    public void buildSeatHeating() {
        seatHeating = new SeatHeating();
    }

    @Override
    public void buildSteering(Steering steering) {
        this.steering = steering;
    }

    @Override
    public Vehicle getVehicle() {
        return new Van(vanChassis, vanEngine, vanWheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }
}
