package ru.javaschool.lesson18.example2.chassis;

public interface Chassis {

    String getChassisParts();
}
