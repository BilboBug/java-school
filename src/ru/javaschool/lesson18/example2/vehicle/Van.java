package ru.javaschool.lesson18.example2.vehicle;

import ru.javaschool.lesson18.example2.brakes.ABS;
import ru.javaschool.lesson18.example2.chassis.Chassis;
import ru.javaschool.lesson18.example2.climate.AirConditioning;
import ru.javaschool.lesson18.example2.climate.ClimateControlSystem;
import ru.javaschool.lesson18.example2.climate.SeatHeating;
import ru.javaschool.lesson18.example2.engine.Engine;
import ru.javaschool.lesson18.example2.steering.Steering;
import ru.javaschool.lesson18.example2.wheels.Wheels;

public class Van extends Vehicle {


    public Van(Chassis chassis, Engine engine, Wheels wheels, Steering steering, ClimateControlSystem climateControlSystem, AirConditioning airConditioning, SeatHeating seatHeating, ABS abs) {
        super(chassis, engine, wheels, steering, climateControlSystem, airConditioning, seatHeating, abs);
    }

    @Override
    public String toString() {
        return "Van{" +
                "chassis=" + getChassis() +
                ", engine=" + getEngine() +
                ", wheels=" + getWheels() +
                ", steering=" + getSteering() +
                ", climateControlSystem=" + getClimateControlSystem() +
                ", airConditioning=" + getAirConditioning() +
                ", seatHeating=" + getSeatHeating() +
                ", abs=" + getAbs() +
                '}';
    }
}
