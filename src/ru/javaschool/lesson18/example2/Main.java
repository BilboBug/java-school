package ru.javaschool.lesson18.example2;

import ru.javaschool.lesson18.example2.Builder.*;
import ru.javaschool.lesson18.example2.vehicle.Vehicle;

public class Main {

    public static void main(String[] args) {
        // Примените шаблон, благодаря которому не нужно будет заполнять все аргументы такого большого конструктора каждый раз при создании объекта

        VehicleDirector minimumEquipmentDirector = new MinimumEquipmentDirector();
        Vehicle standardCar = minimumEquipmentDirector.build(new CarBuilder());
        Vehicle standardTruck = minimumEquipmentDirector.build(new TruckBuilder());
        Vehicle standardVan = minimumEquipmentDirector.build(new VanBuilder());

        VehicleDirector maximumEquipmentDirector = new MaximumEquipmentDirector();
        Vehicle luxCar = maximumEquipmentDirector.build(new CarBuilder());
        Vehicle luxTruck = maximumEquipmentDirector.build(new TruckBuilder());
        Vehicle luxVan = maximumEquipmentDirector.build(new VanBuilder());

        // some action with vehicle
    }


}
