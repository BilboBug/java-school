package ru.javaschool.lesson18.example5;

import java.util.List;

public class Cashier {

    public double calculateSum(List<Purchase> purchases) {
        double resultSum = 0d;
        for (Purchase purchase : purchases) {
            resultSum += purchase.getPrice();
        }

        return resultSum;
    }
}
