package ru.javaschool.lesson18.example5;

import java.util.ArrayList;
import java.util.List;

public class Box implements Purchase {

    // в списке может храниться как Box так и Product
    private List<Purchase> purchases = new ArrayList<>();

    public void addPurchase(Purchase purchase) {
        purchases.add(purchase);
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }

    @Override
    public double getPrice() {
        return purchases.stream().mapToDouble(Purchase::getPrice).sum();
    }
}
