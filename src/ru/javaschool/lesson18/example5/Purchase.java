package ru.javaschool.lesson18.example5;

public interface Purchase {
    double getPrice();
}
