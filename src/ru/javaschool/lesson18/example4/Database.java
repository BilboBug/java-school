package ru.javaschool.lesson18.example4;

public class Database {

    private String address;

    private static class DataBaseInstance {
        public static final Database INSTANCE = new Database();
    }

    private Database() {
    }

    public void init(String address) {
        setAddress(address);
    }

    private void setAddress(String address) {
        this.address = address;
    }

    public static Database getDataBase() {
        return DataBaseInstance.INSTANCE;
    }

    public String getData(long id) {
        // implementation
        return "some value";
    }

    public void saveData(String data, long id) {
        // implementation
    }
}
