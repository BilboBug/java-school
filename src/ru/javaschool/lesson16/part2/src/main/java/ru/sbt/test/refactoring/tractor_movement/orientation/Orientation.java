package ru.sbt.test.refactoring.tractor_movement.orientation;

public enum Orientation {

    NORTH, WEST, SOUTH, EAST;

}
