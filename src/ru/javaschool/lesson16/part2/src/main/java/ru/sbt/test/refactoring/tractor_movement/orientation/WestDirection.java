package ru.sbt.test.refactoring.tractor_movement.orientation;

import ru.sbt.test.refactoring.tractor_movement.Position;
import ru.sbt.test.refactoring.tractor_movement.TractorMovement;

public class WestDirection implements TractorMovement {

    Orientation orientation;
    Position position;

    public WestDirection(Position position) {
        this.position = position;
        orientation = Orientation.WEST;
    }

    public Position nextPosition() {
        position.setX(position.getX() - 1);
        return position;
    }

    public TractorMovement nextOrientation() {
        orientation = Orientation.NORTH;
        return new NorthDirection(position);
    }

    public Position getPosition() {
        return position;
    }

    public Orientation getOrientation() {
        return orientation;
    }
}
