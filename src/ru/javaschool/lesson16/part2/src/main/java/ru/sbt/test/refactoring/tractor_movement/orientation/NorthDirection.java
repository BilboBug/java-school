package ru.sbt.test.refactoring.tractor_movement.orientation;

import ru.sbt.test.refactoring.tractor_movement.Position;
import ru.sbt.test.refactoring.tractor_movement.TractorMovement;

public class NorthDirection implements TractorMovement {

    Orientation orientation;
    Position position;

    public NorthDirection(Position position) {
        this.position = position;
        orientation = Orientation.NORTH;
    }

    public Position nextPosition() {
        position.setY(position.getY() + 1);
        return position;
    }

    public TractorMovement nextOrientation() {
        orientation = Orientation.EAST;
        return new EastDirection(position);
    }

    public Position getPosition() {
        return position;
    }

    public Orientation getOrientation() {
        return orientation;
    }
}
