package ru.sbt.test.refactoring.tractor_movement;

import ru.sbt.test.refactoring.tractor_movement.orientation.Orientation;

public interface TractorMovement {

    Position nextPosition();

    TractorMovement nextOrientation();

    Position getPosition();

    Orientation getOrientation();
}
