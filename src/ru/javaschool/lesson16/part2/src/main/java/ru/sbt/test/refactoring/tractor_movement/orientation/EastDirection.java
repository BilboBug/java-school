package ru.sbt.test.refactoring.tractor_movement.orientation;

import ru.sbt.test.refactoring.tractor_movement.Position;
import ru.sbt.test.refactoring.tractor_movement.TractorMovement;

public class EastDirection implements TractorMovement {

    Orientation orientation;
    Position position;

    public EastDirection(Position position) {
        this.position = position;
        orientation = Orientation.EAST;
    }

    public Position nextPosition() {
        position.setX(position.getX() + 1);
        return position;
    }

    public TractorMovement nextOrientation() {
        orientation = Orientation.SOUTH;
        return new SouthDirection(position);
    }

    public Position getPosition() {
        return position;
    }

    public Orientation getOrientation() {
        return orientation;
    }
}

