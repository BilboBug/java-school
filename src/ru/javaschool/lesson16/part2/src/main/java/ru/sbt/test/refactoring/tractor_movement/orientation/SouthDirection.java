package ru.sbt.test.refactoring.tractor_movement.orientation;

import ru.sbt.test.refactoring.tractor_movement.Position;
import ru.sbt.test.refactoring.tractor_movement.TractorMovement;

public class SouthDirection implements TractorMovement {

    Orientation orientation;
    Position position;

    public SouthDirection(Position position) {
        this.position = position;
        orientation = Orientation.SOUTH;
    }

    public Position nextPosition() {
        position.setY(position.getY() - 1);
        return position;
    }

    public TractorMovement nextOrientation() {
        orientation = Orientation.WEST;
        return new WestDirection(position);
    }

    public Position getPosition() {
        return position;
    }

    public Orientation getOrientation() {
        return orientation;
    }
}
