package ru.sbt.test.refactoring.tractor;

import ru.sbt.test.refactoring.tractor_movement.TractorMovement;
import ru.sbt.test.refactoring.tractor_movement.orientation.Orientation;
import ru.sbt.test.refactoring.tractor_movement.Position;
import ru.sbt.test.refactoring.tractor_movement.orientation.WestDirection;

public class Tractor {

private TractorMovement tractorMovement;
private final Position position = new Position(0, 0);
private final Position field = new Position(5, 5);

    public Tractor() {
        tractorMovement = new WestDirection(position);
    }

    public void move(String command) {
        if (command == "F") {
            moveForwards();
        } else if (command == "T") {
            turnClockwise();
        }
    }

    private void moveForwards() {
        Position position = tractorMovement.nextPosition();
        if (Math.abs(position.getX()) > field.getX() || Math.abs(position.getY()) > field.getY())
            throw new TractorInDitchException();
    }

    private void turnClockwise() {
        tractorMovement = tractorMovement.nextOrientation();
    }

    public int getPositionX() {
        return  position.getX();
    }

    public int getPositionY() {
        return position.getY();
    }

    public Orientation getOrientation() {
        return tractorMovement.getOrientation();
    }
}
