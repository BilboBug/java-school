package ru.sbt.test.refactoring.tractor;

import org.junit.Test;
import ru.sbt.test.refactoring.tractor_movement.orientation.Orientation;

import static org.junit.Assert.*;

public class TractorTest {

    @Test
    public void testShouldMoveForward(){
        Tractor tractor = new Tractor();
        tractor.move("F");
        assertEquals(-1, tractor.getPositionX());
        assertEquals(0, tractor.getPositionY());
    }

    @Test
    public void testShouldTurn(){
        Tractor tractor = new Tractor();
        tractor.move("T");
        assertEquals(Orientation.NORTH, tractor.getOrientation());
        tractor.move("T");
        assertEquals(Orientation.EAST, tractor.getOrientation());
        tractor.move("T");
        assertEquals(Orientation.SOUTH, tractor.getOrientation());
        tractor.move("T");
        assertEquals(Orientation.WEST, tractor.getOrientation());
    }

    @Test
    public void testShouldTurnAndMoveInTheRightDirection(){
        Tractor tractor = new Tractor();
        tractor.move("T");
        tractor.move("T");
        tractor.move("F");
        assertEquals(1, tractor.getPositionX());
        assertEquals(0, tractor.getPositionY());
        tractor.move("T");
        tractor.move("F");
        assertEquals(1, tractor.getPositionX());
        assertEquals(-1, tractor.getPositionY());
        tractor.move("T");
        tractor.move("F");
        assertEquals(0, tractor.getPositionX());
        assertEquals(-1, tractor.getPositionY());
        tractor.move("T");
        tractor.move("F");
        assertEquals(0, tractor.getPositionX());
        assertEquals(0, tractor.getPositionY());
    }

    @Test(expected = TractorInDitchException.class)
    public void testShouldThrowExceptionIfFallsOffPlateau() {
        Tractor tractor = new Tractor();
        tractor.move("F");
        tractor.move("F");
        tractor.move("F");
        tractor.move("F");
        tractor.move("F");
        tractor.move("F");
    }
}