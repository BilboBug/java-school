package ru.sbt.bit.ood.solid.homework;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class SalaryReport {

    private final Connection connection;
    private String sqlRequest;
    private String departmentId;
    private LocalDate dateFrom;
    private LocalDate dateTo;

    public SalaryReport(Connection connection) {
        this.connection = connection;
    }

    public SalaryReport setSqlRequest(String sqlRequest) {
        this.sqlRequest = sqlRequest;
        return this;
    }

    public SalaryReport setRequestParameters(String departmentId, DateRange dateRange) {
        this.departmentId = departmentId;
        this.dateFrom = dateRange.getDateFrom();
        this.dateTo = dateRange.getDateTo();
        return this;
    }

    public ResultSet getResponse() {
        try {
            PreparedStatement ps = connection.prepareStatement(sqlRequest);
            // inject parameters to sql
            ps.setString(0, departmentId);
            ps.setDate(1, new java.sql.Date(dateFrom.toEpochDay()));
            ps.setDate(2, new java.sql.Date(dateTo.toEpochDay()));
            // execute query and get the results
            return ps.executeQuery();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }
}
