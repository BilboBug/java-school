package ru.sbt.bit.ood.solid.homework;

import java.sql.Connection;
import java.sql.ResultSet;
import java.time.LocalDate;

public class SalaryHtmlReportNotifier {

    private final Connection connection;
    private final String host = "mail.google.com";
    private final String subject = "Monthly department salary report";
    private final String sqlRequest = "select emp.id as emp_id, emp.name as amp_name, sum(salary) as salary from employee emp left join" +
            "salary_payments sp on emp.id = sp.employee_id where emp.department_id = ? and" +
            " sp.date >= ? and sp.date <= ? group by emp.id, emp.name";

    public SalaryHtmlReportNotifier(Connection connection) {
        this.connection = connection;
    }

    public void sendReport(String departmentId, LocalDate dateFrom, LocalDate dateTo, String recipients) {
        ResultSet report = new SalaryReport(connection)
                .setSqlRequest(sqlRequest)
                .setRequestParameters(departmentId, new DateRange(dateFrom, dateTo))
                .getResponse();

        HtmlMailSender sender = new HtmlMailSender(host);
        String message = new HtmlFormatter().convertToHtml(report);
        sender.sendMessage(message, subject, recipients);
    }
}
