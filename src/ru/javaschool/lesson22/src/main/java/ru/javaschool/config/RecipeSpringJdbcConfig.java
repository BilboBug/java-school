package ru.javaschool.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import ru.javaschool.dao.RecipesDaoImpl;

import javax.sql.DataSource;

@Configuration
public class RecipeSpringJdbcConfig {

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/homework");
        dataSource.setUsername("postgres");
        dataSource.setPassword("061196a");
        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public RecipesDaoImpl recipesDaoImpl() {
        return new RecipesDaoImpl(jdbcTemplate());
    }
}
