package ru.javaschool.models;

import java.util.List;

public class Recipe {

    private int dishesId;
    private String dishesName;
    List<Ingredient> ingredients;

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getDishesName() {
        return dishesName;
    }

    public void setDishesName(String dishesName) {
        this.dishesName = dishesName;
    }

    public void addIngredient(Ingredient ingredient) {
        ingredients.add(ingredient);
    }

    public int getDishesId() {
        return dishesId;
    }

    public void setDishesId(int dishesId) {
        this.dishesId = dishesId;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "dishesName='" + dishesName + '\'' +
                ", ingredients=" + ingredients +
                '}';
    }
}
