package ru.javaschool.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.javaschool.models.Ingredient;
import ru.javaschool.models.Recipe;

import java.util.List;

@Component
public class RecipesDaoImpl implements RecipesDao {

    JdbcTemplate template;

    @Autowired
    public RecipesDaoImpl(JdbcTemplate template) {
        this.template = template;
    }

    String GET_BY_NAME = "select recipes.id as recipesid, dishes.id as dishesid, dishes.name as dishesname, " +
            "ingredients.id as ingredientid, ingredients.name, recipes.quantity " +
            "from dishes, recipes, ingredients " +
            "where dishes.id = recipes.dishesfk and ingredients.id = recipes.ingredientfk and dishes.name like ?";

    String CREATE_DISH = "INSERT INTO dishes VALUES (?, ?)";
    String CREATE_RECIPE = "INSERT INTO recipes (dishesfk, ingredientfk, quantity) VALUES (?, ?, ?)";
    String UPDATE_RECIPE = "UPDATE recipes SET ingredientfk = ?, quantity = ? WHERE id = ?";
    String DELETE_DISH = "DELETE FROM dishes WHERE id = ?";
    String DELETE_RECIPE = "DELETE FROM recipes WHERE dishesfk = ?";

    @Override
    public void create(Recipe recipe) {
        template.update(CREATE_DISH, recipe.getDishesId(), recipe.getDishesName());
        for (Ingredient ingredient : recipe.getIngredients()) {
            template.update(CREATE_RECIPE, recipe.getDishesId(), ingredient.getId(), ingredient.getQuantity());
        }
    }

    @Override
    public void update(Recipe recipe) {
        for (Ingredient ingredient : recipe.getIngredients()) {
            template.update(UPDATE_RECIPE, ingredient.getId(), ingredient.getQuantity(), ingredient.getRecipeRowId());
        }
    }

    @Override
    public void delete(Recipe recipe) {
        System.out.println(recipe.getIngredients().get(0).getDishesId());
        template.update(DELETE_RECIPE, recipe.getIngredients().get(0).getDishesId());
        template.update(DELETE_DISH, recipe.getDishesId());
    }

    @Override
    public List<Recipe> getRecipesByName(String name) {
        return template.query(GET_BY_NAME, new String[]{"%" + name + "%"}, new RecipesExtractor());
    }
}
