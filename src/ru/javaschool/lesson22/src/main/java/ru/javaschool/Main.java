package ru.javaschool;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.javaschool.config.RecipeSpringJdbcConfig;
import ru.javaschool.dao.RecipesDaoImpl;
import ru.javaschool.models.Ingredient;
import ru.javaschool.models.Recipe;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(RecipeSpringJdbcConfig.class);
        RecipesDaoImpl dao = context.getBean(RecipesDaoImpl.class);

        List<Recipe> recipeList = dao.getRecipesByName("borsh");
        recipeList.forEach(System.out::println);

        Recipe recipe = new Recipe();

        Ingredient meat = new Ingredient("meat");
        meat.setId(2);
        meat.setQuantity(3);
        Ingredient onion = new Ingredient("onion");
        onion.setId(3);
        onion.setQuantity(6);

        List<Ingredient> list = new ArrayList<>();
        list.add(meat);
        list.add(onion);

        recipe.setDishesId(2);
        recipe.setDishesName("onion meat");
        recipe.setIngredients(list);

        dao.create(recipe);
        System.out.println("created onion meat dish");

        List<Recipe> onionMeats = dao.getRecipesByName("onion meat");
        onionMeats.forEach(System.out::println);

        Recipe onionMeat = onionMeats.get(0);
        onionMeat.getIngredients().get(0).setQuantity(90);
        dao.update(onionMeat);
        System.out.println(dao.getRecipesByName(onionMeat.getDishesName()));

        dao.delete(onionMeat);
        System.out.println("onion meat deleted");

        List<Recipe> recipeList2 = dao.getRecipesByName("onion meat");
        if (recipeList2.isEmpty())
            System.out.println("onion meat not found");
    }
}
