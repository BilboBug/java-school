package ru.javaschool.dao;

import ru.javaschool.models.Recipe;

import java.util.List;

public interface RecipesDao {
    List<Recipe> getRecipesByName(String name);

    void delete(Recipe recipe);

    void update(Recipe recipe);

    void create(Recipe recipe);
}
