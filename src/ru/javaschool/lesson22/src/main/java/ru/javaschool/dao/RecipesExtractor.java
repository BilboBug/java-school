package ru.javaschool.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import ru.javaschool.models.Ingredient;
import ru.javaschool.models.Recipe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecipesExtractor implements ResultSetExtractor<List<Recipe>> {
    @Override
    public List<Recipe> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        List<Recipe> recipes = new ArrayList<>();
        Map<String, List<Ingredient>> ingredients = new HashMap<>();
        while (resultSet.next()) {
            Ingredient ingredient = new Ingredient(resultSet.getString("name"));
            ingredient.setDishesName(resultSet.getString("dishesname"));
            ingredient.setDishesId(resultSet.getInt("dishesid"));
            ingredient.setQuantity(resultSet.getInt("quantity"));
            ingredient.setId(resultSet.getInt("ingredientid"));
            ingredient.setRecipeRowId(resultSet.getInt("recipesid"));
            if (ingredients.containsKey(ingredient.getDishesName())) {
                ingredients.get(ingredient.getDishesName()).add(ingredient);
            } else {
                List<Ingredient> list = new ArrayList<>();
                list.add(ingredient);
                ingredients.put(ingredient.getDishesName(), list);
            }
        }
        for (String dishesName : ingredients.keySet()) {
            Recipe recipe = new Recipe();
            recipe.setDishesName(dishesName);
            recipe.setDishesId(ingredients.get(dishesName).get(0).getDishesId());
            recipe.setIngredients(ingredients.get(dishesName));
            recipes.add(recipe);
        }
        return recipes;
    }
}
