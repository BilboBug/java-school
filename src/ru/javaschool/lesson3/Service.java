package ru.javaschool.lesson3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Service {
    public static List<Client> list = new ArrayList<>();
    public int getLoanAmount(Client client) {
        int sum = 0;
        List<Credit> list = client.getCreditList();
        for(Credit credit: list)
            sum += credit.getLoanAmount();
        return sum;
    }
    public List<Client> sortByName() {
        Collections.sort(list, new Comparator<Client>() {
            @Override
            public int compare(Client o1, Client o2) {
                return (o1.getFirstName() + " " + o1.getLastName())
                        .compareTo(o2.getFirstName() + " " + o2.getLastName());
            }
        });
        return list;
    }

    public List<Client> sortByAll() {
        Collections.sort(list, new Comparator<Client>() {
            @Override
            public int compare(Client o1, Client o2) {
                String fullName1 = o1.getFirstName() + " " + o1.getLastName();
                String fullName2 = o2.getFirstName() + " " + o2.getLastName();
                if(fullName1.equals(fullName2))
                    return Integer.compare(getLoanAmount(o2), getLoanAmount(o1));
                else
                    return fullName1.compareTo(fullName2);

            }
        });
        return list;
    }
}
