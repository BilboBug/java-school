package ru.javaschool.lesson3;

public class Credit {
    private static int creditCount = 0;
    private int creditId;
    private int loanAmount;

    public Credit(int loanAmount) {
        this.loanAmount = loanAmount;
        creditId = creditCount;
        creditCount++;
    }

    public int getCreditId() {
        return creditId;
    }

    public void setCreditId(int creditId) {
        this.creditId = creditId;
    }

    @Override
    public String toString() {
        return "Credit{" +
                "creditId=" + creditId +
                ", loanAmount=" + loanAmount +
                '}';
    }

    public int getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(int loanAmount) {
        this.loanAmount = loanAmount;
    }
}
