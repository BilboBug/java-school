package ru.javaschool.lesson3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Client {

    private static int clientCount = 0;
    private int clientId;
    private String firstName;
    private String lastName;
    private List<Credit> creditList = new ArrayList<>();

    public Client(String firstName, String lastName) {
        clientId = clientCount;
        this.firstName = firstName;
        this.lastName = lastName;
        clientCount++;
        Service.list.add(this);
    }

    public void addCredit(List<Credit> credits) {
        creditList.addAll(credits);
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Credit> getCreditList() {
        return creditList;
    }

    @Override
    public String toString() {
        return "Client{" +
                "clientId=" + clientId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", creditList=" + creditList +
                '}';
    }

    public void addCredit(Credit... credits) {
        creditList.addAll(Arrays.asList(credits));
    }
}
