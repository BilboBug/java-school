package ru.javaschool.lesson3;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Credit credit1 = new Credit(1000);
        Credit credit2 = new Credit(2000);
        Credit credit3 = new Credit(3000);
        Client client1 = new Client("Bogdan", "Treskov");
        client1.addCredit(credit1, credit2, credit3);
        System.out.println(client1);
        System.out.println(new Service().getLoanAmount(client1));
        Client client2 = new Client("Gena", "Kolgotov");
        client2.addCredit(new Credit(4000), new Credit(1000));
        Client client3 = new Client("Petya", "Porosenok");
        client3.addCredit(new Credit(500), new Credit(600));
        Client client4 = new Client("German", "Gref");
        Client client5 = new Client("Bogdan", "Treskov");
        client5.addCredit(new Credit(70000));
        System.out.println(new Service().sortByName());
        System.out.println(new Service().sortByAll());
    }
}
