package ru.javaschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.javaschool.models.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
    Client getByPassport_SerialNumber(String serialNumber);
}
