package ru.javaschool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.Client;
import ru.javaschool.models.Transfer;
import ru.javaschool.repository.ClientRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientService {

    private ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Transactional
    public void update(Client client) {
        final Client one = clientRepository.getOne(client.getId());
        one.setTransfers(client.getTransfers());
        one.setPassport(client.getPassport());
        one.setFullName(client.getFullName());
        clientRepository.save(one);
    }

    @Transactional
    public void delete(Client client) {
        clientRepository.delete(client);
    }

    @Transactional
    public void save(Client client) {
        clientRepository.save(client);
    }

    @Transactional
    public Client get(int id) {

        final Client one = clientRepository.getOne(id);
        final Client client = new Client();
        client.setTransfers(one.getTransfers());
        client.setPassport(one.getPassport());
        client.setId(one.getId());
        client.setFullName(one.getFullName());
        return client;
    }

    @Transactional
    public Client getByPassport(String serialNumber) {
        final Client byPassport = clientRepository.getByPassport_SerialNumber(serialNumber);
        final Client client = new Client();
        client.setFullName(byPassport.getFullName());
        client.setId(byPassport.getId());
        client.setPassport(byPassport.getPassport());
        client.setTransfers(byPassport.getTransfers());
        return client;
    }

    public List<Transfer> getTransfersByClientSerialNumber(String serialNumber) {
        return new ArrayList<>(getByPassport(serialNumber).getTransfers());
    }

}
