package ru.javaschool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.Passport;
import ru.javaschool.repository.PassportRepository;

@Service
public class PassportService {

    private final PassportRepository passportRepository;

    @Autowired
    public PassportService(PassportRepository passportRepository) {
        this.passportRepository = passportRepository;
    }

    @Transactional
    public void save(Passport passport) {
        passportRepository.save(passport);
    }

    @Transactional
    public Passport get(int id) {
        return passportRepository.getOne(id);
    }

    @Transactional
    public void update(Passport passport) {
        Passport one = passportRepository.getOne(passport.getId());
        one.setSerialNumber(passport.getSerialNumber());
        one.setClient(passport.getClient());
        passportRepository.save(one);
    }

    @Transactional
    public void delete(Passport passport) {
        passportRepository.delete(passport);
    }
}
