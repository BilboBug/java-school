package ru.javaschool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.javaschool.models.Transfer;
import ru.javaschool.repository.TransferRepository;

@Service
public class TransferService {

    private final TransferRepository transferRepository;

    @Autowired
    public TransferService(TransferRepository transferRepository) {
        this.transferRepository = transferRepository;
    }

    @Transactional
    public void save(Transfer transfer) {
        transferRepository.save(transfer);
    }

    @Transactional
    public Transfer get(int id) {
        return transferRepository.getOne(id);
    }

    @Transactional
    public void update(Transfer transfer) {
        Transfer one = transferRepository.getOne(transfer.getId());
        one.setAmount(transfer.getAmount());
        transferRepository.save(one);
    }

    @Transactional
    public void delete(Transfer transfer) {
        transferRepository.delete(transfer);
    }
}
