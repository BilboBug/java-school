package ru.javaschool;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.javaschool.config.ApplicationConfig;
import ru.javaschool.models.Client;
import ru.javaschool.models.Transfer;
import ru.javaschool.service.ClientService;

import java.util.List;

public class MainSpringData {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        ClientService clientService = context.getBean(ClientService.class);
        Client client = clientService.get(1);
        System.out.println(client.getFullName());

        client.setFullName("updated name");
        clientService.update(client);

        final Client client1 = clientService.getByPassport("88005553535");
        System.out.println(client1.getFullName());

        final List<Transfer> transfers = clientService.getTransfersByClientSerialNumber("88005553535");
        transfers.forEach(transfer -> System.out.println(transfer.getAmount()));
    }
}
