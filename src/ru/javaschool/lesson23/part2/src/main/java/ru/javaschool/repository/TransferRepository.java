package ru.javaschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.javaschool.models.Transfer;

@Repository
public interface TransferRepository extends JpaRepository<Transfer, Integer> {
}
