package ru.javaschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.javaschool.models.Passport;

@Repository
public interface PassportRepository extends JpaRepository<Passport, Integer> {
}
