package hibernateUtil;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryAccess {

    private static final class SessionInstance {
        public final static SessionFactory sessionFactory = build();

        private static SessionFactory build() {
            return new Configuration().configure().buildSessionFactory();
        }
    }

    public static SessionFactory getSessionFactory() {
        return SessionInstance.sessionFactory;
    }
}
