package dao;

import models.Passport;

public interface PassportDao extends Dao<Passport> {
}
