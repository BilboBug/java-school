package service;

import dao.TransferDao;
import models.Client;
import models.Transfer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class TransferDaoImpl implements TransferDao {

    private final SessionFactory sessionFactory;

    public TransferDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Transfer transfer) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(transfer);
            session.getTransaction().commit();
        }
    }

    public Transfer read(int id) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            final Transfer transfer = session.get(Transfer.class, id);
            session.getTransaction().commit();
            return transfer;
        }
    }

    public void update(Transfer transfer) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(transfer);
            session.getTransaction().commit();
        }
    }

    public void delete(Transfer transfer) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(transfer);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Transfer> getTransfersByClientPassport(String serialNumber) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            String s = "from Client where passport.serialNumber = :serialNumber";
            Client client = session.createQuery
                    (s, Client.class)
                    .setParameter("serialNumber", serialNumber)
                    .getResultList().stream().findAny().orElse(null);
            session.getTransaction().commit();
            return client.getTransfers();
        }
    }
}
