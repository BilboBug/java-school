package dao;

import models.Transfer;

import java.util.List;

public interface TransferDao extends Dao<Transfer> {
    List<Transfer> getTransfersByClientPassport(String serialNumber);
}
