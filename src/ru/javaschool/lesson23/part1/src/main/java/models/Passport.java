package models;

import javax.persistence.*;

@Entity
@Table(name = "passport")
public class Passport {
    @Id @GeneratedValue
    private int id;
    private String serialNumber;
    @OneToOne(mappedBy = "passport")
    private Client client;

    public Passport() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
