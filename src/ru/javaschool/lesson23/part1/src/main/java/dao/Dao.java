package dao;

public interface Dao<T> {
    void save(T t);

    T read(int id);

    void update(T t);

    void delete(T t);
}
