import dao.ClientDao;
import dao.PassportDao;
import dao.TransferDao;
import hibernateUtil.SessionFactoryAccess;
import models.Client;
import models.Passport;
import models.Transfer;
import org.hibernate.SessionFactory;
import service.ClientDaoImpl;
import service.PassportDaoImpl;
import service.TransferDaoImpl;

import java.util.ArrayList;
import java.util.List;

public class MainHibernate {
    public static void main(String[] args) {
        try (SessionFactory sessionFactory = SessionFactoryAccess.getSessionFactory()) {
            ClientDao clientDao = new ClientDaoImpl(sessionFactory);
            PassportDao passportDao = new PassportDaoImpl(sessionFactory);
            TransferDao transferDao = new TransferDaoImpl(sessionFactory);

            Client client = new Client();
            client.setFullName("Michael Ivanovich");

            Passport passport = new Passport();
            passport.setClient(client);
            passport.setSerialNumber("88005553535");

            Transfer transfer = new Transfer();
            transfer.setAmount(1000);
            List<Transfer> list = new ArrayList<>();
            list.add(transfer);

            client.setPassport(passport);
            client.setTransfers(list);

            passportDao.save(passport);
            transferDao.save(transfer);
            clientDao.save(client);

            Client client1 = clientDao.read(1);
            List<Transfer> transfers = client1.getTransfers();
            for (Transfer transfer1 : transfers)
                System.out.println(transfer1.getAmount());

            System.out.println(client1.getPassport().getSerialNumber());

            client1.setFullName("updated name");
            clientDao.update(client1);

            Client client2 = clientDao.getByPassportNumber("88005553535");
            System.out.println(client2.getFullName());

            List<Transfer> transfers1 = transferDao.getTransfersByClientPassport("88005553535");
            for (Transfer t : transfers1)
                System.out.println(t.getAmount());

        }


    }
}
