package service;

import dao.PassportDao;
import models.Passport;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class PassportDaoImpl implements PassportDao {

    private final SessionFactory sessionFactory;

    public PassportDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Passport passport) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(passport);
            session.getTransaction().commit();
        }
    }

    public Passport read(int id) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            final Passport passport = session.get(Passport.class, id);
            session.getTransaction().commit();
            return passport;
        }
    }

    public void update(Passport passport) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(passport);
            session.getTransaction().commit();
        }
    }

    public void delete(Passport passport) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(passport);
            session.getTransaction().commit();
        }
    }
}
