package service;

import dao.ClientDao;
import models.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class ClientDaoImpl implements ClientDao {

    private final SessionFactory sessionFactory;

    public ClientDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Client client) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(client);
            session.getTransaction().commit();
        }
    }

    public Client read(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            final Client client = session.get(Client.class, id);
            session.getTransaction().commit();
            return client;
        }
    }

    public void update(Client client) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(client);
            session.getTransaction().commit();
        }
    }

    public void delete(Client client) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(client);
            session.getTransaction().commit();
        }
    }

    @Override
    public Client getByPassportNumber(String serialNumber) {
        try (final Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            List<Client> clients = session.createQuery("from Client where passport.serialNumber = :serialNumber", Client.class)
                    .setParameter("serialNumber", serialNumber)
                    .getResultList();
            session.getTransaction().commit();
            return clients.stream().findAny().orElse(null);
        }
    }
}
