package dao;

import models.Client;

public interface ClientDao extends Dao<Client> {
    Client getByPassportNumber(String serialNumber);
}
