package ru.javaschool.lesson4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class CountMapImpl<T> implements CountMap<T> {

    private List<Collector<T>> list = new ArrayList<>();
    private List<T> keys = new ArrayList<>();

    @Override
    public void add(T o) {
        add(o, 1);
    }

    private void add(T o, Integer i) {
        if(keys.contains(o)) {
            for (Collector<T> collector : list)
                if (collector.getKey().equals(o)) {
                    collector.setValue(collector.getValue() + i);
                    break;
                }
        } else {
            list.add(new Collector<T>(o, i));
            keys.add(o);
        }
    }

    @Override
    public int getCount(T o) {
        for(Collector<T> collector: list)
            if(collector.getKey().equals(o))
                return collector.getValue();
        return 0;
    }

    @Override
    public int remove(T o) {
        int count = getCount(o);
        list.removeIf(new Predicate<Collector<T>>() {
            @Override
            public boolean test(Collector<T> tCollector) {
                return tCollector.getKey().equals(o);
            }
        });
        keys.remove(o);
        return count;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public void addAll(CountMap<T> source) {
        for(T key: source.toMap().keySet())
            add(key, source.getCount(key));
    }

    @Override
    public Map<T, Integer> toMap() {
        Map<T, Integer> map = new HashMap<>();
        for(Collector<T> collector: list)
            map.put(collector.getKey(), collector.getValue());
        return map;
    }

    @Override
    public void toMap(Map<T, Integer> destination) {
        destination.clear();
        destination.putAll(toMap());
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for(Collector<T> collector: list)
            res.append(collector).append("; ");
        return res.toString();
    }
}
