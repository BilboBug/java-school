package ru.javaschool.lesson4;

import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String[] args) {

        CountMap<Integer> map = new CountMapImpl<>();
        CountMap<Integer> map1 = new CountMapImpl<>();
        map.add(10);
        map.add(10);
        map.add(5);
        map.add(6);
        map.add(5);
        map.add(10);

        map1.add(6);
        map1.add(100);
        map1.add(100);
        map1.add(89);
        map1.add(6);
        map1.add(17);
        map1.add(6);

        System.out.println(map.getCount(5));
        System.out.println(map.getCount(6));
        System.out.println(map.getCount(10));
        System.out.println(map.getCount(1));
        System.out.println(map.size());

        map.add(7);
        map.add(7);
        System.out.println(map.remove(7));
        System.out.println(map.remove(1));

        System.out.println(map);
        System.out.println(map1);
        map.addAll(map1);
        System.out.println(map);


        Map<Integer, Integer> toMap = new HashMap<>();
        toMap.put(10, 100);
        toMap.put(100, 10);
        toMap.put(77, 6);
        System.out.println(toMap);
        map.toMap(toMap);
        System.out.println(toMap);
        System.out.println(map.toMap());


    }

}
