package ru.javaschool.lesson4;

import java.util.Objects;

public class Collector<T> {
    Integer value;
    T key;

    public Collector(T key, Integer value) {
        this.value = value;
        this.key = key;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public T getKey() {
        return key;
    }

    public void setKey(T key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Collector<?> collector = (Collector<?>) o;
        return Objects.equals(value, collector.value) && Objects.equals(key, collector.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, key);
    }

    @Override
    public String toString() {
        return "{" +
                "key=" + key +
                ", value=" + value +
                '}';
    }
}
