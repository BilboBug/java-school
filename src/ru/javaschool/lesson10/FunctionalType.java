package ru.javaschool.lesson10;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

public class FunctionalType<T, V> {

    private Predicate<? super T> predicate;
    private Function<? super T, ? extends V> function;

    public FunctionalType(Predicate<? super T> predicate) {
        this.predicate = predicate;
        function = null;
    }

    public FunctionalType(Function<? super T, ? extends V> function) {
        this.function = function;
        predicate = null;
    }

    public Predicate<? super T> getPredicate() {
        return predicate;
    }

    public void setPredicate(Predicate<? super T> predicate) {
        this.predicate = predicate;
    }

    public Function<? super T, ? extends V> getFunction() {
        return function;
    }

    public void setFunction(Function<? super T, ? extends V> function) {
        this.function = function;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FunctionalType<?, ?> that = (FunctionalType<?, ?>) o;
        return Objects.equals(predicate, that.predicate) && Objects.equals(function, that.function);
    }

    @Override
    public int hashCode() {
        return Objects.hash(predicate, function);
    }
}
