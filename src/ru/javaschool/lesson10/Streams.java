package ru.javaschool.lesson10;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class Streams<T> {

    private final List<T> list;
    private final List<FunctionalType> funcList;

    public Streams(List<T> list) {
        this.list = new ArrayList<>(list);
        funcList = new ArrayList<>();
    }

    public static <R> Streams<R> of(List<R> list) {
        return new Streams<>(list);
    }

    public Streams<T> filter(Predicate<? super T> predicate) {
        funcList.add(new FunctionalType<T, T>(predicate));
        return this;
    }

    public <V> Streams<V> transform(Function<? super T, ? extends V> function) {
        funcList.add(new FunctionalType<>(function));
        return (Streams<V>) this;
    }

    public <K, V, R> Map<K, V> toMap(Function<T, K> keyFunction, Function<T, V> valueFunction) {
        Map<K, V> map = new HashMap<>();
        for (FunctionalType<R, T> funct : funcList) {
            if(funct.getPredicate() != null) {
                for (int i = list.size() - 1; i > -1; i--)
                    if (!funct.getPredicate().test((R) list.get(i)))
                        list.remove(i);
            } else if(funct.getFunction() != null) {
                for(int i = 0; i < list.size(); i++) {
                    T res = funct.getFunction().apply((R) list.get(i));
                    list.set(i, res);
                }
            }
        }
        for(T t: list)
            map.put(keyFunction.apply(t), valueFunction.apply(t));
        return map;
    }
}
