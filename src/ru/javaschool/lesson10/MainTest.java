package ru.javaschool.lesson10;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainTest {
    public static void main(String[] args) {

        List<Person> list = new ArrayList<>();
        list.add(new Person("Sylvanas", 10));
        list.add(new Person("Garrosh", 50));
        list.add(new Person("Illidan", 90));

        Map<String, Person> map = Streams.of(list)
                                            .filter(p -> p.getAge() > 33)
                                            .transform(p -> new Person(p.getName(), p.getAge() + 10))
                                            .filter(p -> p.getAge() < 100)
                                            .toMap(p -> p.getName(), p -> p);
        
        for(Person p: map.values())
            System.out.println(p.getName() + p.getAge());

        list.forEach(person -> System.out.println(person.getName() + person.getAge()));
    }
}
