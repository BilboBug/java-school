package ru.javaschool.lesson9.cache;

import ru.javaschool.lesson9.annotations.CacheType;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class LoaderImpl implements Loader {

    Map<String, Double> hardWorkCache = new HashMap<>();
    Map<String, List<String>> workCache = new HashMap<>();

    String cacheDir = "src/ru/javaschool/lesson9/resultcache";

    @Override
    public boolean isCached(String name, CacheType type, boolean zip) {
        if (type.equals(CacheType.IN_MEMORY))
            return hardWorkCache.containsKey(name) || workCache.containsKey(name);
        else if (type.equals(CacheType.FILE)) {
            File file;
            if (zip)
                file = new File(cacheDir, name + ".zip");
            else
                file = new File(cacheDir, name);
            return file.exists();
        }
        return false;
    }

    @Override
    public Object get(String name, CacheType type, boolean zip) {
        System.out.println("in loader");
        if (type.equals(CacheType.IN_MEMORY)) {
            if (hardWorkCache.containsKey(name))
                return hardWorkCache.get(name);
            if (workCache.containsKey(name))
                return workCache.get(name);
        } else if (type.equals(CacheType.FILE)) {

            Object result;
            if (zip) {
                File file = unzipFile(new File(cacheDir, name + ".zip"));
                result = readResult(file);
                file.delete();
            } else {
                File file = new File(cacheDir, name);
                result = readResult(file);
            }
            return result;
        }
        return null;
    }

    @Override
    public void cache(String name, Object result, CacheType type, boolean zip) {
        if (type.equals(CacheType.IN_MEMORY)) {
            if (result.getClass().equals(Double.class)) {
                hardWorkCache.put(name, (Double) result);
            }
            if (result.getClass().getSimpleName().equals("ArrayList")) {
                workCache.put(name, (List<String>) result);
            }
        } else if (type.equals(CacheType.FILE)) {
            File file = new File(cacheDir, name);
            try {
                file.createNewFile();
                writeResult(file, result);
                if (zip)
                    zipFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void writeResult(File file, Object result) throws IOException {
        try (OutputStream outputStream = new FileOutputStream(file);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(result);
        }
    }

    private Object readResult(File file) {
        System.out.println("from file");
        try (InputStream inputStream = new FileInputStream(file);
             ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
            return objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void zipFile(File file) {
        try (ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(cacheDir + "/" + file.getName() + ".zip"));
             FileInputStream fis = new FileInputStream(file)) {
            ZipEntry entry1 = new ZipEntry(file.getName());
            zout.putNextEntry(entry1);

            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            zout.write(buffer);
            zout.closeEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }
        file.delete();
    }

    private File unzipFile(File file) {
        try (ZipInputStream zin = new ZipInputStream(new FileInputStream(file))) {
            ZipEntry entry;
            String name;
            if ((entry = zin.getNextEntry()) != null) {
                name = entry.getName();
                File file1 = new File(cacheDir, name);
                FileOutputStream fout = new FileOutputStream(file1);
                for (int i = zin.read(); i != -1; i = zin.read()) {
                    fout.write(i);
                }
                fout.flush();
                zin.closeEntry();
                fout.close();
                System.out.println("unzipped");
                return file1;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
