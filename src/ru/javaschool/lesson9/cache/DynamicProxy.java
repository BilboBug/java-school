package ru.javaschool.lesson9.cache;

import ru.javaschool.lesson9.annotations.Cache;
import ru.javaschool.lesson9.service.Service;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DynamicProxy implements InvocationHandler {

    private final Map<String, Cache> map;
    private Loader loader;
    private final Service service;

    public DynamicProxy(Map<String, Cache> map, Service service) {
        this.map = map;
        this.service = service;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Cache cache = map.get(method.getName());
        String key = cache.fileNamePrefix() + method.getName();
        for (int i = 0; i < args.length; i++) {
            key += cache.identityBy()[i].getSimpleName() + args[i];
        }

        System.out.println(key);

        if (loader.isCached(key, cache.cacheType(), cache.zip())) {
            return loader.get(key, cache.cacheType(), cache.zip());
        } else {
            Object res = method.invoke(service, args);
            loader.cache(key, setResult(res), cache.cacheType(), cache.zip());
            return res;
        }
    }

    private Object setResult(Object res) {
        if (res.getClass().getSimpleName().equals("ArrayList")) {
            Cache cache = map.get("work");
            List<String> list = (List<String>) res;
            int size = cache.listSize();
            if (size != 0)
                list = list.subList(0, size);
            return new ArrayList<>(list);
        }
        return res;
    }

    public void setLoader(Loader loader) {
        this.loader = loader;
    }
}
