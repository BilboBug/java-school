package ru.javaschool.lesson9.cache;


import ru.javaschool.lesson9.annotations.CacheType;

public interface Loader {

    boolean isCached(String name, CacheType type, boolean zip);

    Object get(String name, CacheType type, boolean zip);

    void cache(String name, Object result, CacheType type, boolean zip);

}
