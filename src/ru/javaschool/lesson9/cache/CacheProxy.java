package ru.javaschool.lesson9.cache;

import ru.javaschool.lesson9.annotations.Cache;
import ru.javaschool.lesson9.service.Service;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class CacheProxy {

    private DynamicProxy dynamicProxy;

    private final Map<String, Cache> map = new HashMap<>();

    public Map<String, Cache> getMap() {
        return map;
    }

    public Service cache(Service service) {
        for (Method method : service.getClass().getInterfaces()[0].getDeclaredMethods()) {
            Cache annotation = method.getDeclaredAnnotation(Cache.class);
            map.put(method.getName(), annotation);
        }
        dynamicProxy = new DynamicProxy(map, service);
        return (Service) Proxy.newProxyInstance(service.getClass().getClassLoader(),
                service.getClass().getInterfaces(),
                dynamicProxy);
    }

    public Loader cache(Loader loader) {
        dynamicProxy.setLoader(loader);
        return loader;
    }
}
