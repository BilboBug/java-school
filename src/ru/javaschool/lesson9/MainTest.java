package ru.javaschool.lesson9;

import ru.javaschool.lesson9.cache.CacheProxy;
import ru.javaschool.lesson9.cache.LoaderImpl;
import ru.javaschool.lesson9.service.Service;
import ru.javaschool.lesson9.service.ServiceImpl;

public class MainTest {
    public static void main(String[] args) {

        CacheProxy cacheProxy = new CacheProxy();
        Service service = cacheProxy.cache(new ServiceImpl());
        cacheProxy.cache(new LoaderImpl());

        System.out.println(service.doHardWork("doHardWork", 3));
        System.out.println(service.doHardWork("doHardWork", 3));
        System.out.println(service.doHardWork("doHardWork", 2));
        System.out.println(service.doHardWork("doHardWork", 2));
        System.out.println(service.work("dofirstwork"));
        System.out.println(service.work("dofirstwork"));
        System.out.println(service.work("dosecondwork"));
        System.out.println(service.work("dosecondwork"));
    }
}
