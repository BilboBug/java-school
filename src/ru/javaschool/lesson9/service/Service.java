package ru.javaschool.lesson9.service;

import ru.javaschool.lesson9.annotations.Cache;
import ru.javaschool.lesson9.annotations.CacheType;

import java.util.List;

public interface Service {

    @Cache(cacheType = CacheType.FILE, fileNamePrefix = "test", zip = true, identityBy = {String.class, Integer.class})
    double doHardWork(String s, Integer i);

    @Cache(cacheType = CacheType.FILE, fileNamePrefix = "test2", zip = false, identityBy = {String.class}, listSize = 3)
    List<String> work(String s);

}
