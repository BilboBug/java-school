package ru.javaschool.lesson9.service;

import java.util.Arrays;
import java.util.List;

public class ServiceImpl implements Service {

    @Override
    public double doHardWork(String s, Integer i) {
        System.out.println("in service");
        return (double) s.length() / i;
    }

    @Override
    public List<String> work(String s) {
        System.out.println("in service");
        return Arrays.asList(s.split(""));
    }

}
