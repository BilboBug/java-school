package ru.javaschool.lesson9.annotations;

public enum CacheType {
    FILE,
    IN_MEMORY
}
