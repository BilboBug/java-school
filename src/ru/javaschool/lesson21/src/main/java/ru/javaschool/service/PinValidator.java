package ru.javaschool.service;

import org.springframework.stereotype.Service;
import ru.javaschool.exceptions.AccountBlockedException;
import ru.javaschool.exceptions.WrongPasswordException;
import ru.javaschool.model.AccountStage;
import ru.javaschool.model.BankAccount;
import ru.javaschool.model.Client;
import ru.javaschool.model.ValidationStage;

@Service
public class PinValidator {

    public void validate(Client client, String password) throws WrongPasswordException, AccountBlockedException {
        BankAccount account = client.getAccount();
        AccountStage accountStage = account.getAccountStage();
        if (accountStage.getStage().equals(ValidationStage.BLOCKED)) {
            long time;
            if ((time = System.currentTimeMillis() - accountStage.getBlockTime()) < 5000) {
                long l = time / 1000;
                throw new AccountBlockedException(
                        "account blocked:" + Math.round(5 - l) + " seconds remaining"
                );
            }
            else {
                accountStage.setStage(ValidationStage.ALLOW);
                accountStage.setBlockTime(0);
            }
        }
        if (!account.showPassword().equals(password)) {
            client.setAuth(false);
            if (accountStage.getStage().equals(ValidationStage.ALLOW)) {
                accountStage.incrementCount();
                accountStage.setStage(ValidationStage.FORBID);
                throw new WrongPasswordException(
                        "wrong password: " + (3 - accountStage.getWrongPasswordCount()) + " attempts remaining"
                );
            }
            if (accountStage.getStage().equals(ValidationStage.FORBID)) {
                accountStage.incrementCount();
                if (accountStage.getWrongPasswordCount() > 2) {
                    accountStage.setStage(ValidationStage.BLOCKED);
                    accountStage.setWrongPasswordCount(0);
                    accountStage.setBlockTime(System.currentTimeMillis());
                    throw new AccountBlockedException("account blocked: 5 seconds remaining");
                }
                throw new WrongPasswordException(
                        "wrong password: " + (3 - accountStage.getWrongPasswordCount()) + " attempts remaining"
                );
            }
        }
        else {
            accountStage.setStage(ValidationStage.ALLOW);
            accountStage.setWrongPasswordCount(0);
        }
    }
}
