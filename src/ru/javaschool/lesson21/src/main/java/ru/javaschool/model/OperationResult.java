package ru.javaschool.model;

public class OperationResult {
    private int status;
    private String operation;
    private String result;

    public OperationResult(int status, String operation, String result) {
        this.status = status;
        this.operation = operation;
        this.result = result;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "OperationResult{" +
                "result='" + result + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
