package ru.javaschool.terminal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.javaschool.exceptions.AccountBlockedException;
import ru.javaschool.exceptions.WrongPasswordException;
import ru.javaschool.model.Client;
import ru.javaschool.server.TerminalServer;
import ru.javaschool.service.PinValidator;

@Service
public class TerminalImpl implements Terminal {

    TerminalServer terminalServer;
    PinValidator validator;

    @Autowired
    public TerminalImpl(TerminalServer terminalServer, PinValidator validator) {
        this.terminalServer = terminalServer;
        this.validator = validator;
    }

    @Override
    public void getAccess(String id, String password) throws AccountBlockedException, WrongPasswordException {
        Client client = terminalServer.getClient(id);
        validator.validate(client, password);
        client.setAuth(true);
    }

    @Override
    public void putCash(String id, int amount) throws IllegalAccessException {
        Client client = terminalServer.getClient(id);
        if (client.isAuth()) {
            client.getAccount().addAmount(amount);
            client.setAuth(false);
        }
        else {
            throw new IllegalAccessException("user not authorized");
        }
    }

    @Override
    public void takeCash(String id, int amount) throws IllegalAccessException {
        Client client = terminalServer.getClient(id);
        if (client.isAuth()) {
            client.getAccount().takeAmount(amount);
            client.setAuth(false);
        }
        else {
            throw new IllegalAccessException("user not authorized");
        }
    }

    @Override
    public Client getClient(String id) throws IllegalAccessException {
        Client client = terminalServer.getClient(id);
        if (client.isAuth()) {
            client.setAuth(false);
            return client;
        }
        else throw new IllegalAccessException("user not authorized");
    }
}
