package ru.javaschool.model;

public class AccountStage {

    private ValidationStage stage;
    private long blockTime;
    private int wrongPasswordCount;

    public ValidationStage getStage() {
        return stage;
    }

    public void setStage(ValidationStage stage) {
        this.stage = stage;
    }

    public long getBlockTime() {
        return blockTime;
    }

    public void setBlockTime(long blockTime) {
        this.blockTime = blockTime;
    }

    public int getWrongPasswordCount() {
        return wrongPasswordCount;
    }

    public void setWrongPasswordCount(int wrongPasswordCount) {
        this.wrongPasswordCount = wrongPasswordCount;
    }

    public int incrementCount() {
        return ++wrongPasswordCount;
    }
}
