package ru.javaschool.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.javaschool.exceptions.AccountBlockedException;
import ru.javaschool.exceptions.OperationException;
import ru.javaschool.exceptions.WrongPasswordException;
import ru.javaschool.model.Client;
import ru.javaschool.model.OperationResult;
import ru.javaschool.terminal.Terminal;

import java.util.Map;

@RestController
@RequestMapping("/terminal")
public class TerminalController {

    private final Terminal terminal;

    @Autowired
    public TerminalController(Terminal terminal) {
        this.terminal = terminal;
    }

    @PostMapping("/auth")
    public OperationResult auth(@RequestParam String id, @RequestParam String password) {
        try {
            terminal.getAccess(id, password);
        } catch (AccountBlockedException | WrongPasswordException e) {
            throw new OperationException(e.getMessage());
        }
        return new OperationResult(HttpStatus.OK.value(), "authentication", "success");
    }

    @GetMapping("/account/{id}")
    public Client getAccountInfo(@PathVariable String id) {
        try {
            return terminal.getClient(id);
        } catch (IllegalAccessException e) {
            throw new OperationException("authorization required");
        }
    }

    @PostMapping("/putCash/{id}")
    public OperationResult makeDeposit(@RequestParam int amount, @PathVariable String id) {
        try {
            terminal.putCash(id, amount);
        } catch (IllegalAccessException e) {
            throw new OperationException("authorization required");
        }
        return new OperationResult(HttpStatus.OK.value(), "deposit", "success");
    }

    @PostMapping("/getCash/{id}")
    public OperationResult makeWithdraw(@RequestParam int amount, @PathVariable String id) {
        try {
            terminal.takeCash(id, amount);
        } catch (IllegalAccessException e) {
            throw new OperationException("authorization required");
        }
        return new OperationResult(HttpStatus.OK.value(), "withdraw", "success");
    }
}
