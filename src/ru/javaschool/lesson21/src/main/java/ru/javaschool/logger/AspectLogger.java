package ru.javaschool.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectLogger {

    @AfterReturning(returning = "returningValue", value = "execution(* ru.javaschool.controller.*.*(..))")
    public void recordSuccessfulExecution(JoinPoint joinPoint, Object returningValue) {
        System.out.printf(
                "method - %s in class - %s successfully executed with result - %s\n",
                joinPoint.getSignature().getName(),
                joinPoint.getSourceLocation().getWithinType().getName(),
                returningValue
        );
    }

    @AfterThrowing(throwing = "exception", value = "execution(* ru.javaschool.controller.*.*(..))")
    public void recordFailedExecution(JoinPoint joinPoint, Exception exception) {
        System.out.printf(
                "method - %s in class - %s throw exception - %s\n",
                joinPoint.getSignature().getName(),
                joinPoint.getSourceLocation().getWithinType().getName(),
                exception
        );
    }
}
