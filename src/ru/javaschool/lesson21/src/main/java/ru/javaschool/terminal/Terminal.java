package ru.javaschool.terminal;

import ru.javaschool.exceptions.AccountBlockedException;
import ru.javaschool.exceptions.WrongPasswordException;
import ru.javaschool.model.Client;

public interface Terminal {

    void getAccess(String id, String password) throws AccountBlockedException, WrongPasswordException;

    void putCash(String id, int amount) throws IllegalAccessException;

    void takeCash(String id, int amount) throws IllegalAccessException;

    Client getClient(String id) throws IllegalAccessException;
}
