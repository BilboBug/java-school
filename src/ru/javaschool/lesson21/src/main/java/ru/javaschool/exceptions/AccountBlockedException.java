package ru.javaschool.exceptions;

public class AccountBlockedException extends Exception {

    public AccountBlockedException() {
    }

    public AccountBlockedException(String s) {
        super(s);
    }
}
