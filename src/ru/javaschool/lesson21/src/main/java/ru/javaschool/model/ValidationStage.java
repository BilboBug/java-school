package ru.javaschool.model;

public enum ValidationStage {
    ALLOW,
    FORBID,
    BLOCKED
}
