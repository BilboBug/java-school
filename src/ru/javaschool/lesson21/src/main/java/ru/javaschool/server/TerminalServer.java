package ru.javaschool.server;

import org.springframework.stereotype.Service;
import ru.javaschool.model.AccountStage;
import ru.javaschool.model.BankAccount;
import ru.javaschool.model.Client;
import ru.javaschool.model.ValidationStage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class TerminalServer {

    private Map<String, Client> clients = new ConcurrentHashMap<>();

    public TerminalServer() {
        Client client1 = new Client();
        client1.setFullName("Leha Navalny");
        client1.setId("1");
        BankAccount bankAccount = new BankAccount();
        bankAccount.setAmount(3500);
        bankAccount.setPassword("1234");
        bankAccount.setStage(new AccountStage());
        client1.setAccount(bankAccount);
        bankAccount.getAccountStage().setStage(ValidationStage.ALLOW);
        clients.put("1", client1);
    }

    public Client getClient(String id) {
        return clients.get(id);
    }

    public void addClient(Client client) {
        clients.put(client.getId(), client);
    }
}
