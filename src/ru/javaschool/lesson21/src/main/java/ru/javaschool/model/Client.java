package ru.javaschool.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Client {

    @JsonIgnore
    private String id;
    private String fullName;
    private BankAccount account;
    @JsonIgnore
    private boolean auth;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public BankAccount getAccount() {
        return account;
    }

    public void setAccount(BankAccount account) {
        this.account = account;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public boolean isAuth() {
        return auth;
    }

    @Override
    public String toString() {
        return "Client{" +
                "fullName='" + fullName + '\'' +
                ", account=" + account +
                '}';
    }
}
