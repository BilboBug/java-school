package ru.javaschool.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BankAccount {
    @JsonIgnore
    private String password;
    private int amount;
    @JsonIgnore
    private AccountStage accountStage;

    public String showPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void addAmount(int amount) {
        this.amount += amount;
    }

    public void takeAmount(int amount) {
        this.amount -= amount;
    }

    public AccountStage getAccountStage() {
        return accountStage;
    }

    public void setStage(AccountStage accountStage) {
        this.accountStage = accountStage;
    }

    @Override
    public String toString() {
        return "{" +
                "amount=" + amount +
                '}';
    }
}
