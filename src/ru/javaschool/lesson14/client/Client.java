package ru.javaschool.lesson14.client;

public class Client {
    public static void main(String[] args) {
        new ClientApp("localhost", 9999).begin();
    }
}
