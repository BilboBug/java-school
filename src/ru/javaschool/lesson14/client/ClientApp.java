package ru.javaschool.lesson14.client;

import ru.javaschool.lesson14.client.connect.MessageReader;
import ru.javaschool.lesson14.client.connect.MessageWriter;

import java.io.*;
import java.net.Socket;

public class ClientApp {

    private BufferedReader inSocket;
    private BufferedReader userInput;
    private BufferedWriter outSocket;

    public ClientApp(String address, int port) {
        try {
            Socket socket = new Socket(address, port);
            userInput = new BufferedReader(new InputStreamReader(System.in));
            inSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outSocket = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void begin() {

        Thread readThread = new Thread(new MessageReader(inSocket));
        Thread writeThread = new Thread(new MessageWriter(userInput, outSocket));
        readThread.start();
        writeThread.start();

    }


}
