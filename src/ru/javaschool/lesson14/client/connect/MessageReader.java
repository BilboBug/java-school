package ru.javaschool.lesson14.client.connect;

import java.io.BufferedReader;
import java.io.IOException;

public class MessageReader implements Runnable {
    BufferedReader in;

    public MessageReader(BufferedReader in) {
        this.in = in;
    }

    @Override
    public void run() {
        String str;
        try {
            while (true) {
                str = in.readLine();
                if (str.equals("stop")) {
                    in.close();
                    break;
                }
                System.out.println(str);
            }
        } catch (IOException ignored) {
        }
    }
}
