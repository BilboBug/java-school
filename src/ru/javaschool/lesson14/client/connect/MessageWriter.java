package ru.javaschool.lesson14.client.connect;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class MessageWriter implements Runnable {

    BufferedReader userInput;
    BufferedWriter out;

    public MessageWriter(BufferedReader userInput, BufferedWriter out) {
        this.userInput = userInput;
        this.out = out;
    }

    @Override
    public void run() {
        while (true) {
            String userWord;
            try {
                userWord = userInput.readLine();
                if (userWord.equals("stop")) {
                    out.write("stop" + "\n");
                    out.close();
                    break;
                } else {
                    out.write(userWord + "\n");
                }
                out.flush();
            } catch (IOException ignored) {
            }
        }
    }
}
