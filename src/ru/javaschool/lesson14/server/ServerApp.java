package ru.javaschool.lesson14.server;

import java.io.*;

public class ServerApp implements Runnable {

    BufferedReader in;
    BufferedWriter out;
    String name;
    boolean isAuthorized = false;

    public ServerApp(InputStream in, OutputStream out) {
        this.in = new BufferedReader(new InputStreamReader(in));
        this.out = new BufferedWriter(new OutputStreamWriter(out));
    }

    public void authorize() throws IOException {
        while (!isAuthorized) {
            send("write 1 if you want to auth on server or 2 to register");
            String answer = in.readLine();
            if (answer.equals("1")) {
                send("write your login");
                String name = in.readLine();
                if (Server.connectionsMap.containsKey(name)) {
                    this.name = name;
                    isAuthorized = true;
                    send("you authorized");
                    sendForAll("System>>" + name + " connected to the chat");
                }
            } else if (answer.equals("2")) {
                send("write new login");
                String name = in.readLine();
                this.name = name;
                isAuthorized = true;
                Server.connectionsMap.put(name, this);
                send("you authorized");
                sendForAll("System>>" + name + " connected to the chat");
            }
        }
    }

    @Override
    public void run() {
        try {
            if (!isAuthorized) {
                authorize();
            }
            while (true) {
                String word = in.readLine();
                if (word.equals("stop")) {
                    sendForAll("System>>" + name + " leave this chat");
                    break;
                }
                System.out.println(name + ": " + word);
                String[] message;
                if ((message = getUserNameFromMessage(word)) != null)
                    sendForUser(message[0], "from: " + name + ">>" + message[1]);
                else
                    sendForAll(name + ">>" + word);
            }
        } catch (NullPointerException ignored) {

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void send(String word) {
        try {
            out.write(word + "\n");
            out.flush();
        } catch (IOException ignored) {
        }
    }

    private void sendForAll(String message) {
        for (ServerApp con : Server.connectionsMap.values()) {
            if (!con.equals(this))
                con.send(message);
        }
    }

    private void sendForUser(String name, String message) {
        if (Server.connectionsMap.containsKey(name)) {
            Server.connectionsMap.get(name).send(message);
        } else {
            send("user with this name not found");
        }
    }

    private String[] getUserNameFromMessage(String message) {
        if (message.matches("^\\[.+].+")) {
            String name = message.substring(message.indexOf('[') + 1, message.lastIndexOf(']'));
            String word = message.substring(message.lastIndexOf(']') + 1);
            return new String[]{name, word};
        }
        return null;
    }
}
