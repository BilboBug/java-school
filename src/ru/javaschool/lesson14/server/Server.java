package ru.javaschool.lesson14.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

public class Server {


    private static final int PORT = 9999;

    public static ConcurrentHashMap<String, ServerApp> connectionsMap = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        try (ServerSocket server = new ServerSocket(PORT)) {
            System.out.println("Server Started");
            while (true) {
                Socket socket = server.accept();
                try {
                    ServerApp con = new ServerApp(socket.getInputStream(), socket.getOutputStream());
                    new Thread(con).start();
                } catch (IOException e) {
                    e.printStackTrace();
                    socket.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
