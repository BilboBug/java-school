package ru.javaschool.lesson5.servers;

import java.util.HashMap;
import java.util.Map;

public class TerminalServer {
    private Map<String, BankAccount> map;

    public TerminalServer() {
        map = new HashMap<>();
    }

    public BankAccount getAccount(String clientId) {
        return map.get(clientId);
    }

    public void addAccount(String clientId, BankAccount bankAccount) {
        map.put(clientId, bankAccount);
    }
}
