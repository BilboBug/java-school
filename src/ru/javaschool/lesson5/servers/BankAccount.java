package ru.javaschool.lesson5.servers;

import java.util.UUID;

public class BankAccount {
    String id;
    Integer amount;
    String password;

    public BankAccount(String password) {
        this.password = password;
        id = UUID.randomUUID().toString();
        amount = 0;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

}
