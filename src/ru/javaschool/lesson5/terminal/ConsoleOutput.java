package ru.javaschool.lesson5.terminal;

import ru.javaschool.lesson5.exceptions.AccountBlockedException;
import ru.javaschool.lesson5.exceptions.IncorrectValueOfPaymentsException;
import ru.javaschool.lesson5.exceptions.InsufficientFundsException;
import ru.javaschool.lesson5.exceptions.WrongPasswordException;
import ru.javaschool.lesson5.servers.Client;
import ru.javaschool.lesson5.servers.TerminalServer;

import java.util.Scanner;

public class ConsoleOutput {

    Terminal terminal;
    TerminalServer server;
    Scanner scanner = new Scanner(System.in);

    public ConsoleOutput(TerminalServer server) {
        this.server = server;
    }

    public void start(Client client) {
        String text = "";
        while (!text.equals("=")) {
            init(client);
            System.out.println("Введи что-нибудь для продолжения, или = для завершения");
            scanner = new Scanner(System.in);
            text = scanner.nextLine();
        }
    }

    private void init(Client client) {
        try {
            System.out.println("Введи пароль: ");
            scanner = new Scanner(System.in);
            String password = scanner.nextLine();
            if(password.equals("cancel"))
                return;
            terminal = new TerminalImpl(server, client, password);
            System.out.println("Авторизация прошла успешно.\n" +
                    "Введи - 1, 2 или 3, чтобы узнать кол-во средств,\n" +
                    "положить деньги на счет или снять соответсвенно");
            String text = "";
            while(!text.equals("back")) {
                scanner = new Scanner(System.in);
                text = scanner.nextLine();

                if (text.equals("1")) {
                    System.out.println(showClientsMoney(client));
                } else if (text.equals("2")) {
                    System.out.println("Введи сумму: ");
                    scanner = new Scanner(System.in);
                    try {
                        System.out.println(putMoney(client, scanner.nextInt()));
                    } catch (IncorrectValueOfPaymentsException e) {
                        System.out.println("Сумма должна быть кратна 100");
                    }
                } else if (text.equals("3")) {
                    System.out.println("Введи сумму: ");
                    scanner = new Scanner(System.in);
                    try {
                        System.out.println(getMoney(client, scanner.nextInt()));
                    } catch (IncorrectValueOfPaymentsException e) {
                        System.out.println("Сумма должна быть кратна 100");
                    } catch (InsufficientFundsException e) {
                        System.out.println("Недостаточно средств");
                    }
                }
            }
        } catch (AccountBlockedException e) {
            System.out.println("Счет временно заблокирован, осталось сукунд: " + e.getMessage());
            init(client);
        } catch (WrongPasswordException e) {
            System.out.println("Не верный пароль. Осталось попыток: " + e.getMessage());
            init(client);
        }
    }

    private String showClientsMoney(Client client) {
        Integer amount = terminal.showAmount(client);
        return "У тебя " + amount + " юаней";
    }

    private String putMoney(Client client, Integer money) throws IncorrectValueOfPaymentsException {
        terminal.putMoney(client, money);
        return "Зачислено " + money + " юаней";
    }

    private   String getMoney(Client client, Integer money) throws IncorrectValueOfPaymentsException, InsufficientFundsException {
        terminal.getMoney(client, money);
        return "Списанно " + money + " юаней";
    }
}
