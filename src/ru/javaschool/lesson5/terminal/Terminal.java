package ru.javaschool.lesson5.terminal;

import ru.javaschool.lesson5.exceptions.IncorrectValueOfPaymentsException;
import ru.javaschool.lesson5.exceptions.InsufficientFundsException;
import ru.javaschool.lesson5.servers.Client;

public interface Terminal {
    Integer showAmount(Client client);
    void putMoney(Client client, Integer amount) throws IncorrectValueOfPaymentsException;
    void getMoney(Client client, Integer amount) throws IncorrectValueOfPaymentsException, InsufficientFundsException;
}
