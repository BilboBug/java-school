package ru.javaschool.lesson5.terminal;

import ru.javaschool.lesson5.exceptions.AccountBlockedException;
import ru.javaschool.lesson5.exceptions.IncorrectValueOfPaymentsException;
import ru.javaschool.lesson5.exceptions.InsufficientFundsException;
import ru.javaschool.lesson5.exceptions.WrongPasswordException;
import ru.javaschool.lesson5.servers.BankAccount;
import ru.javaschool.lesson5.servers.Client;
import ru.javaschool.lesson5.servers.TerminalServer;
import ru.javaschool.lesson5.service.PinValidator;

import java.util.HashMap;
import java.util.Map;

public class TerminalImpl implements Terminal {
    TerminalServer server;
    static PinValidator validator = new PinValidator();
    String password;
    static Map<String, Integer> wrongPasswordsCount = new HashMap<>();

    public TerminalImpl(TerminalServer server, Client client, String password) throws WrongPasswordException, AccountBlockedException {
        this.server = server;
        validator.validate(server.getAccount(client.getId()), password);
    }

    @Override
    public Integer showAmount(Client client) {
        return server.getAccount(client.getId()).getAmount();
    }

    @Override
    public void putMoney(Client client, Integer amount) throws IncorrectValueOfPaymentsException {
        if(amount % 100 != 0)
            throw new IncorrectValueOfPaymentsException();
        BankAccount bankAccount = server.getAccount(client.getId());
        Integer currentAmount = bankAccount.getAmount();
        bankAccount.setAmount(currentAmount + amount);
    }

    @Override
    public void getMoney(Client client, Integer amount) throws IncorrectValueOfPaymentsException, InsufficientFundsException {
        if(amount % 100 != 0)
            throw new IncorrectValueOfPaymentsException();
        BankAccount bankAccount = server.getAccount(client.getId());
        if(amount > bankAccount.getAmount())
            throw new InsufficientFundsException();
        bankAccount.setAmount(bankAccount.getAmount() - amount);
    }
}
