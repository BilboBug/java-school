package ru.javaschool.lesson5.service;

import ru.javaschool.lesson5.exceptions.AccountBlockedException;
import ru.javaschool.lesson5.exceptions.WrongPasswordException;
import ru.javaschool.lesson5.servers.BankAccount;

import java.util.*;

public class PinValidator {

    private Map<String, Integer> wrongPasswordsCount;
    private Map<String, Long> lockedAccounts;



    public PinValidator() {
        wrongPasswordsCount = new HashMap<>();
        lockedAccounts = new HashMap<>();
    }

    public void validate(BankAccount bankAccount, String password) throws WrongPasswordException, AccountBlockedException {
       if(howMuchSecondsRemaining(bankAccount) > 0) {
           throw new AccountBlockedException(howMuchSecondsRemaining(bankAccount).toString());
       } else if(!isPasswordCorrect(bankAccount, password)) {
           if(getWrongPasswordsCount(bankAccount) > 1) {
               lockedAccounts.put(bankAccount.getId(), System.currentTimeMillis() + 5000L);
               wrongPasswordsCount.remove(bankAccount.getId());
               throw new AccountBlockedException(howMuchSecondsRemaining(bankAccount).toString());
           }
           wrongPasswordsCount.put(bankAccount.getId(), getWrongPasswordsCount(bankAccount) + 1);
           throw new WrongPasswordException(Integer.toString (3 - getWrongPasswordsCount(bankAccount)));
       } else
           wrongPasswordsCount.remove(bankAccount.getId());
    }

    private Integer howMuchSecondsRemaining(BankAccount bankAccount) {
        Long currentTime = System.currentTimeMillis();
        Long unlockTime = lockedAccounts.getOrDefault(bankAccount.getId(), 0L);
        if(unlockTime > currentTime)
            return Math.toIntExact((unlockTime - currentTime)/1000);
        lockedAccounts.remove(bankAccount.getId());
        return 0;
    }

    private boolean isPasswordCorrect(BankAccount bankAccount, String password) {
        if(bankAccount.getPassword().equals(password))
            return true;
        else return false;
    }

    public Integer getWrongPasswordsCount(BankAccount bankAccount) {
        return wrongPasswordsCount.getOrDefault(bankAccount.getId(), 0);
    }
}
