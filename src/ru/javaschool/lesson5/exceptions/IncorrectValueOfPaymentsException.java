package ru.javaschool.lesson5.exceptions;

public class IncorrectValueOfPaymentsException extends Exception{

    public IncorrectValueOfPaymentsException() {
    }

    public IncorrectValueOfPaymentsException(String message) {
        super(message);
    }

    public IncorrectValueOfPaymentsException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectValueOfPaymentsException(Throwable cause) {
        super(cause);
    }
}
