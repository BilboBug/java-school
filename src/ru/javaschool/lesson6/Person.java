package ru.javaschool.lesson6;

import ru.javaschool.lesson6.annotations.*;

import java.util.Date;

public class Person {

    @NotNull
    @NotEmpty
    @MaxLength(32)// не должно быть пустым, не более 32 символов
    private String name;
    @NotNull
    @NotEmpty
    @MaxLength(32)// не должно быть пустым, не более 32 символов
    private String surname;
    @NotNull
    @MaxLength(32)// может быть пустым, не более 32 символов
    private String middleName;
    @NotNull
    private Date birthdayDate;
    @Min(0)// только положительные значения
    private int age;
    @MinLength(6)// не менее 6 символов
    private String password;

    public Person(String name, String surname, String middleName, Date birthdayDate, int age, String password) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthdayDate = birthdayDate;
        this.age = age;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public int getAge() {
        return age;
    }

    public String getPassword() {
        return password;

    }

}
