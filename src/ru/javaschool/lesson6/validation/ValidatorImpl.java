package ru.javaschool.lesson6.validation;

import ru.javaschool.lesson6.annotations.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

public class ValidatorImpl<T> implements Validator<T> {

    @Override
    public void validate(T obj) throws Exception {
        Field[] fields = obj.getClass().getDeclaredFields();
        for(Field field: fields) {
            field.setAccessible(true);
            Annotation[] annotations = field.getAnnotations();
            for(Annotation annotation: annotations) {
                if(annotation instanceof NotNull) {
                    if(field.get(obj) == null)
                        throw new NullPointerException(field.getName() + " is null");
                }
                if(annotation instanceof NotEmpty) {
                    if(field.get(obj) instanceof String)
                        if(((String) field.get(obj)).length() == 0)
                            throw new IllegalArgumentException(field.getName() + " не должно быть пустым");
                }
                if(annotation instanceof MinLength) {
                    if(field.get(obj) instanceof String)
                        if(((String) field.get(obj)).length() < ((MinLength) annotation).value())
                            throw new Exception("Минимальная длина " + field.getName() + ": " + ((MinLength) annotation).value());
                }
                if(annotation instanceof MaxLength) {
                    if (field.get(obj) instanceof String)
                        if (((String) field.get(obj)).length() > ((MaxLength) annotation).value())
                            throw new Exception("Максимальная длина " + field.getName() + ": " + ((MaxLength) annotation).value());
                }
                if(annotation instanceof Min) {
                    if (field.getInt(obj) < ((Min) annotation).value())
                        throw new Exception("Минимальное значение " + field.getName() + ": " + ((Min) annotation).value());
                }
                if(annotation instanceof Max) {
                    if(field.getInt(obj) > ((Max) annotation).value())
                        throw new Exception("Максимальное значение " + field.getName() + ": " + ((Max) annotation).value());
                }

            }
        }
    }
}
