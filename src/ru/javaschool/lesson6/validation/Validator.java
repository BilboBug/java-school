package ru.javaschool.lesson6.validation;

public interface Validator<T> {
    void validate(T obj) throws Exception;
}
