package ru.javaschool.lesson6;

import ru.javaschool.lesson6.validation.Validator;
import ru.javaschool.lesson6.validation.ValidatorImpl;

import java.util.Date;

public class Test {
    public static void main(String[] args) throws Exception {
        Person person = new Person("Bogdan", "tresk", "", new Date(), 18, "123456");
        Validator<Person> validator = new ValidatorImpl<>();
        validator.validate(person);
    }
}
