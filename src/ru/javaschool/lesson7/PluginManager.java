package ru.javaschool.lesson7;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

public class PluginManager {
    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    public Plugin load(String pluginName, String pluginClassName) throws ClassNotFoundException {
        CustomClassLoader loader = new CustomClassLoader();
        Class<Plugin> clazz = loader.findClass(pluginRootDirectory + "." +
                pluginName + "." + pluginClassName);
        Plugin plugin = null;
        try {
            plugin = clazz.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
            throw new ClassNotFoundException();
        }
        return plugin;
    }
}
