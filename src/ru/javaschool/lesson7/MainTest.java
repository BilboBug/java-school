package ru.javaschool.lesson7;

import java.io.File;

public class MainTest {
    public static void main(String[] args) throws ClassNotFoundException {
        PluginManager manager = new PluginManager("ru.javaschool.lesson7.resources");
        manager.load("example", "ExamplePlugin").doSomething();

    }
}
