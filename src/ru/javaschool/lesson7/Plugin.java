package ru.javaschool.lesson7;

public interface Plugin {
    public void doSomething();
}
