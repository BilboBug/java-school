package ru.javaschool;

public class Test {
    public static void main(String[] args) {
        Client client1 = new Client("Treskov Bogdan");
        Client client2 = new Client("Vadimov Vadim");

        BankAccount account1 = new BankAccount("1234");
        BankAccount account2 = new BankAccount("4321");

        TerminalServer server = new TerminalServer();
        server.addAccount(client1.getId(), account1);
        server.addAccount(client2.getId(), account2);

        ConsoleOutput out = new ConsoleOutput(server);
        out.start(client1);

    }
}
