package ru.javaschool;

import ru.javaschool.Client;
import ru.javaschool.IncorrectValueOfPaymentsException;
import ru.javaschool.InsufficientFundsException;

public interface Terminal {
    Integer showAmount(Client client);
    void putMoney(Client client, Integer amount) throws IncorrectValueOfPaymentsException;
    void getMoney(Client client, Integer amount) throws IncorrectValueOfPaymentsException, InsufficientFundsException;
}
