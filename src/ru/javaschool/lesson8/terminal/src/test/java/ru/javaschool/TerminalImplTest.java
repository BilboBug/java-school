package ru.javaschool;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TerminalImplTest {

    private TerminalServer server;
    private Client client;
    private TerminalImpl terminal;
    private BankAccount account;

    @Before
    public void init() throws AccountBlockedException, WrongPasswordException {
        account = new BankAccount("1234");
        account.setAmount(100);
        server = new TerminalServer();
        client = new Client("Full Name");
        server.addAccount(client.getId(), account);
        terminal = new TerminalImpl(server, client, "1234");
    }

    @Test
    public void showAmount() {
        Integer actual = terminal.showAmount(client);
        assertEquals(Integer.valueOf(100), actual);
    }

    @Test
    public void putMoney() throws IncorrectValueOfPaymentsException {
        terminal.putMoney(client, 100);
        Integer actual = terminal.showAmount(client);
        assertEquals(Integer.valueOf(200), actual);
    }

    @Test(expected = IncorrectValueOfPaymentsException.class)
    public void putMoneyIncorrectValue() throws IncorrectValueOfPaymentsException {
        terminal.putMoney(client, 150);
    }

    @Test
    public void getMoney() throws IncorrectValueOfPaymentsException, InsufficientFundsException {
        terminal.getMoney(client, 100);
        Integer actual = terminal.showAmount(client);
        assertEquals(Integer.valueOf(0), actual);
    }

    @Test(expected = IncorrectValueOfPaymentsException.class)
    public void getMoneyIncorrectValue() throws IncorrectValueOfPaymentsException, InsufficientFundsException {
        terminal.getMoney(client, 50);
    }

    @Test(expected = InsufficientFundsException.class)
    public void getMoneyInsufficientFounds() throws IncorrectValueOfPaymentsException, InsufficientFundsException {
        terminal.getMoney(client, 200);
    }
}