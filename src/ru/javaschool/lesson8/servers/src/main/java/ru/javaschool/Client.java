package ru.javaschool;

import java.util.UUID;

public class Client {
    String id;
    String fullName;

    public Client(String fullName) {
        this.fullName = fullName;
        id = UUID.randomUUID().toString();
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getId() {
        return id;
    }
}
