package ru.javaschool;

public class AccountBlockedException extends Exception{

    public AccountBlockedException() {
    }

    public AccountBlockedException(String message) {
        super(message);
    }

    public AccountBlockedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountBlockedException(Throwable cause) {
        super(cause);
    }
}
