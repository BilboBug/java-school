package ru.javaschool;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class PinValidatorTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void validateWrongPassword() throws AccountBlockedException, WrongPasswordException {
        exception.expect(WrongPasswordException.class);
        exception.expectMessage("2");
        PinValidator validator = new PinValidator();
        BankAccount account = new BankAccount("12345");
        validator.validate(account, "123");
    }

    @Test
    public void validateAccountBlocked() throws AccountBlockedException {
        exception.expect(AccountBlockedException.class);
        PinValidator validator = new PinValidator();
        BankAccount account = new BankAccount("1234");
        try {
            validator.validate(account, "12");
            fail("WrongPasswordException expected");
        } catch (WrongPasswordException e) {
            try {
                validator.validate(account, "12");
                fail("WrongPasswordException expected");
            } catch (WrongPasswordException wrongPasswordException) {
                try {
                    validator.validate(account, "12");
                    fail("AccountBlockedException expected");
                } catch (WrongPasswordException passwordException) {
                    fail("AccountBlockedException expected");
                }
            }
        }
    }

    @Test
    public void validate() throws AccountBlockedException, WrongPasswordException {
        PinValidator validator = new PinValidator();
        BankAccount account = new BankAccount("1234");
        validator.validate(account, "1234");
    }
}