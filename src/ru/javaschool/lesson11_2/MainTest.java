package ru.javaschool.lesson11_2;

import ru.javaschool.lesson11_2.fixedpool.FixedThreadPool;
import ru.javaschool.lesson11_2.interfacepool.ThreadPool;
import ru.javaschool.lesson11_2.scalablepool.ScalableThreadPool;

public class MainTest {
    public static void main(String[] args) throws InterruptedException {
        ThreadPool pool = new FixedThreadPool(2);

        pool.execute(new Task("Task 1"));
        pool.execute(new Task("Task 2"));
        pool.execute(new Task("Task 3"));
        pool.start();

        Thread.sleep(3000);

        pool.execute(new Task("Task 1"));
        pool.execute(new Task("Task 2"));
        pool.execute(new Task("Task 3"));

        pool.stop();

        System.out.println("----------------");

        ThreadPool scalableThreadPool = new ScalableThreadPool(2, 4);

        scalableThreadPool.execute(new Task("Task 1"));
        scalableThreadPool.execute(new Task("Task 2"));
        scalableThreadPool.execute(new Task("Task 3"));
        scalableThreadPool.execute(new Task("Task 4"));

        scalableThreadPool.start();

        Thread.sleep(2000);

        scalableThreadPool.execute(new Task("Task 5"));
        scalableThreadPool.execute(new Task("Task 6"));
        scalableThreadPool.execute(new Task("Task 7"));
        scalableThreadPool.execute(new Task("Task 8"));
        scalableThreadPool.execute(new Task("Task 9"));
        scalableThreadPool.execute(new Task("Task 10"));
        scalableThreadPool.execute(new Task("Task 11"));

        Thread.sleep(2000);

        scalableThreadPool.stop();
    }
}
