package ru.javaschool.lesson11_2.scalablepool;

import ru.javaschool.lesson11_2.Wrapper;
import ru.javaschool.lesson11_2.interfacepool.ThreadPool;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class ScalableThreadPool implements ThreadPool {

    private final List<Thread> threads;
    private final Queue<Runnable> tasks = new ArrayDeque<>();
    private final int max;
    private final int min;
    private final Wrapper<Boolean> flag = new Wrapper<>(false);

    public ScalableThreadPool(int min, int max) {
        threads = new ArrayList<>();
        for (int i = 0; i < min; i++) {
            threads.add(new Thread(new ScalableThread(tasks, flag, threads, min)));
        }
        this.max = max;
        this.min = min;
    }

    @Override
    public void execute(Runnable runnable) {
        synchronized (tasks) {
            tasks.add(runnable);
            tasks.notify();
        }
        synchronized (flag) {
            flag.notify();
        }
    }

    @Override
    public void start() {
        Thread sizeController = new Thread(() -> {
            while (!flag.getUnit()) {
                while (threads.size() < tasks.size() && threads.size() < max) {
                    Thread thread = new Thread(new ScalableThread(tasks, flag, threads, min));
                    threads.add(thread);
                    System.out.println(thread.getName() + " started in controller");
                    thread.start();
                }
                synchronized (flag) {
                    try {
                        System.out.println("cont wait");
                        flag.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        sizeController.start();
        for (int i = 0; i < min; i++) {
            System.out.println(threads.get(i).getName() + " started");
            threads.get(i).start();
        }
    }

    @Override
    public void stop() {
        synchronized (tasks) {
            flag.setUnit(true);
            tasks.notifyAll();
        }
        synchronized (flag) {
            flag.notify();
        }
    }
}
