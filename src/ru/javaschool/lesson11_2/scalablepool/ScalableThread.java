package ru.javaschool.lesson11_2.scalablepool;

import ru.javaschool.lesson11_2.Wrapper;

import java.util.List;
import java.util.Queue;

public class ScalableThread implements Runnable {

    private final Queue<Runnable> tasks;
    private final Wrapper<Boolean> flag;
    private final List<Thread> threads;
    private final int min;

    public ScalableThread(Queue<Runnable> tasks, Wrapper<Boolean> flag, List<Thread> threads, int min) {
        this.tasks = tasks;
        this.flag = flag;
        this.threads = threads;
        this.min = min;
    }

    @Override
    public void run() {
        while (!flag.getUnit()) {
            Runnable runnable;
            synchronized (tasks) {
                while (tasks.isEmpty()) {
                    try {
                        if (threads.size() > min || flag.getUnit()) {
                            threads.remove(Thread.currentThread());
                            return;
                        }
                        System.out.println(Thread.currentThread().getName() + " waiting for task");
                        tasks.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runnable = tasks.poll();
            }
            runnable.run();
        }
    }
}
