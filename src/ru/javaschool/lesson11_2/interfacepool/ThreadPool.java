package ru.javaschool.lesson11_2.interfacepool;

public interface ThreadPool {
    void execute(Runnable runnable);

    void start();

    void stop();
}
