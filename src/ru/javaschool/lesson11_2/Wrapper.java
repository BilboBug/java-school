package ru.javaschool.lesson11_2;

public class Wrapper<T> {
    T unit;

    public Wrapper(T unit) {
        this.unit = unit;
    }

    public T getUnit() {
        return unit;
    }

    public void setUnit(T unit) {
        this.unit = unit;
    }
}
