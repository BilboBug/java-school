package ru.javaschool.lesson11_2.fixedpool;

import ru.javaschool.lesson11_2.Wrapper;

import java.util.Queue;

public class FixedThread implements Runnable {

    private final Queue<Runnable> tasks;
    private final Wrapper<Boolean> stop;

    public FixedThread(Queue<Runnable> tasks, Wrapper<Boolean> stop) {
        this.tasks = tasks;
        this.stop = stop;
    }

    @Override
    public void run() {
        while (!stop.getUnit()) {
            Runnable runnable;
            synchronized (tasks) {
                while (tasks.isEmpty()) {
                    try {
                        if (stop.getUnit())
                            return;
                        tasks.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runnable = tasks.poll();
            }
            runnable.run();
        }
    }
}
