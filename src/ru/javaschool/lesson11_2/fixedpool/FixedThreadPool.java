package ru.javaschool.lesson11_2.fixedpool;

import ru.javaschool.lesson11_2.Wrapper;
import ru.javaschool.lesson11_2.interfacepool.ThreadPool;

import java.util.ArrayDeque;
import java.util.Queue;

public class FixedThreadPool implements ThreadPool {

    Thread[] threads;
    private final Queue<Runnable> tasks = new ArrayDeque<>();
    private final Wrapper<Boolean> stop = new Wrapper<>(false);

    public FixedThreadPool(int numberOfThreads) {
        threads = new Thread[numberOfThreads];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(new FixedThread(tasks, stop));
        }

    }

    @Override
    public void execute(Runnable runnable) {
        synchronized (tasks) {
            tasks.add(runnable);
            tasks.notify();
        }
    }

    @Override
    public void start() {
        for (Thread t : threads)
            t.start();
    }

    @Override
    public void stop() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (tasks) {
            stop.setUnit(true);
            tasks.notifyAll();
        }
    }

}
