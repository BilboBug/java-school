package ru.javaschool.lesson11_2;

public class Task implements Runnable {

    String message;

    public Task(String message) {
        this.message = message;
    }

    @Override
    public void run() {
        System.out.println(message + " in " + Thread.currentThread().getName());
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
