package ru.javaschool.lesson13;

public interface Stack<T> {
    void push(T element);
    T pop();
}
