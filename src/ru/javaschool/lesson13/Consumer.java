package ru.javaschool.lesson13;

public class Consumer implements Runnable {

    Stack<Integer> stack;

    public Consumer(Stack<Integer> stack) {
        this.stack = stack;
    }

    @Override
    public void run() {
        for (int i = 0; i < 30; i++) {
            System.out.print(stack.pop());
        }
    }
}
