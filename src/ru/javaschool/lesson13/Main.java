package ru.javaschool.lesson13;

public class Main {

    public static void main(String[] args) {

        Stack<Integer> stack = new MyStack<>(5);

        Thread prod1 = new Thread(new Producer(stack));
        Thread prod2 = new Thread(new Producer(stack));
        Thread prod3 = new Thread(new Producer(stack));

        Thread cons = new Thread(new Consumer(stack));

        prod1.start();
        prod2.start();
        prod3.start();
        cons.start();
    }
}
