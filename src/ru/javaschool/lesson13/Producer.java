package ru.javaschool.lesson13;

public class Producer implements Runnable {

    Stack<Integer> stack;

    public Producer(Stack<Integer> stack) {
        this.stack = stack;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            stack.push(i);
        }
    }
}
