package ru.javaschool.lesson13;

import java.util.ArrayDeque;
import java.util.Queue;

public class MyStack<T> implements Stack<T> {

    private Queue<T> queue;
    private final Integer size;
    Object pop = new Object();
    Object push = new Object();

    public MyStack(int size) {
        queue = new ArrayDeque<>(size);
        this.size = size;
    }

    public void push(T element) {
        while (queue.size() >= size) {
            synchronized (push) {
                try {
                    push.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized (pop) {
            queue.add(element);
            pop.notify();
        }
    }

    public T pop() {
        T element;
        while (queue.isEmpty()) {
            synchronized (pop) {
                try {
                    pop.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        synchronized (push) {
            element = queue.poll();
            push.notify();
        }
        return element;
    }

}
