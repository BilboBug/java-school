package ru.javaschool.lesson12.part2;

import ru.javaschool.lesson12.part2.interfaces.Context;
import ru.javaschool.lesson12.part2.interfaces.ExecutionManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class ExecutionManagerImpl implements ExecutionManager {

    private final AtomicInteger completeTaskCount = new AtomicInteger(0);
    private final AtomicInteger failedTaskCount = new AtomicInteger(0);
    private final AtomicInteger interruptedTaskCount = new AtomicInteger(0);
    private final AtomicBoolean isInterrupted = new AtomicBoolean(false);
    private final AtomicBoolean isFinished = new AtomicBoolean(false);

    private final AtomicInteger finishedTaskCount = new AtomicInteger(0);

    private Runnable[] tasks;
    private Runnable callback;

    private final ExecutorService service = Executors.newFixedThreadPool(4);

    public ExecutionManagerImpl() {
    }

    @Override
    public Context execute(Runnable callback, Runnable... tasks) {
        this.tasks = tasks;
        this.callback = callback;
        for (Runnable r : tasks)
            service.submit(genRunnable(r));
        return setContext();
    }

    private Runnable genRunnable(Runnable task) {
        return () -> {
            if (isInterrupted.get()) {
                interruptedTaskCount.incrementAndGet();
            } else {
                try {
                    task.run();
                    completeTaskCount.incrementAndGet();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    failedTaskCount.incrementAndGet();
                }
            }
            if (finishedTaskCount.incrementAndGet() == tasks.length) {
                callback.run();
                isFinished.set(true);
                service.shutdown();
            }
        };
    }

    private Context setContext() {
        return new Context() {
            @Override
            public int getCompleteTaskCount() {
                return completeTaskCount.get();
            }

            @Override
            public int getFailedTaskCount() {
                return failedTaskCount.get();
            }

            @Override
            public int getInterruptedTaskCount() {
                return interruptedTaskCount.get();
            }

            @Override
            public void interrupt() {
                isInterrupted.set(true);
            }

            @Override
            public boolean isFinished() {
                return isFinished.get();
            }
        };
    }
}
