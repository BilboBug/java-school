package ru.javaschool.lesson12.part2;

public class TestRunnable implements Runnable {

    String message;

    public TestRunnable(String message) {
        this.message = message;
    }

    @Override
    public void run() {
        System.out.println(message + " in " + Thread.currentThread().getName());
    }
}
