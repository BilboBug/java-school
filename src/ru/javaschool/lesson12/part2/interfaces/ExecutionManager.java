package ru.javaschool.lesson12.part2.interfaces;

public interface ExecutionManager {
    Context execute(Runnable callback, Runnable... tasks);
}
