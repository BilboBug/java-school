package ru.javaschool.lesson12.part2.interfaces;

public interface Context {
    int getCompleteTaskCount();

    int getFailedTaskCount();

    int getInterruptedTaskCount();

    void interrupt();

    boolean isFinished();
}
