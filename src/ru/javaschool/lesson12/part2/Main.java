package ru.javaschool.lesson12.part2;

import ru.javaschool.lesson12.part2.interfaces.Context;
import ru.javaschool.lesson12.part2.interfaces.ExecutionManager;

public class Main {
    public static void main(String[] args) {
        ExecutionManager manager = new ExecutionManagerImpl();
        Runnable[] tasks = new Runnable[8];
        for (int i = 0; i < tasks.length; i++) {
            tasks[i] = new TestRunnable("task" + i);
        }
        Context context = manager.execute(new TestRunnable("callback"), tasks);
       // context.interrupt();
//        while (!context.isFinished()) { }
//        System.out.println(context.getCompleteTaskCount() + " " + context.getInterruptedTaskCount());
    }
}
