package ru.javaschool.lesson12.part1;

import java.util.concurrent.Callable;

public class MyCallable implements Callable<String> {

    String message;

    public MyCallable(String message) {
        this.message = message;
    }

    @Override
    public String call() {
        return message + " in " + Thread.currentThread().getName();
    }
}
