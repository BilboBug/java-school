package ru.javaschool.lesson12.part1;

public class TaskExecutor<T> implements Runnable {

    Task<T> task;

    public TaskExecutor(Task<T> task) {
        this.task = task;
    }

    @Override
    public void run() {
        try {
            System.out.println(task.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
