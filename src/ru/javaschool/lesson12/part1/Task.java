package ru.javaschool.lesson12.part1;

import java.util.concurrent.Callable;

public class Task<T> {

    Callable<? extends T> callable;
    private volatile CalculateException exception;
    volatile T result;
    private final Object key = new Object();

    public Task(Callable<? extends T> callable) {
        this.callable = callable;
    }

    public T get() {
        if (result == null) {
            synchronized (key) {
                if (result == null) {
                    try {
                        result = callable.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                        exception = new CalculateException(e);
                        throw exception;
                    }
                } else {
                    if (exception != null)
                        throw exception;
                    else
                        System.out.println("Calculated result: " + result + " in " + Thread.currentThread().getName());
                }
                return result;
            }
        } else {
            System.out.println("Calculated result out of synchronized block: " + result + " in " + Thread.currentThread().getName());
            return result;
        }
    }
}
