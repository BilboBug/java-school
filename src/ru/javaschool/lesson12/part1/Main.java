package ru.javaschool.lesson12.part1;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        MyCallable myCallable = new MyCallable("task");
        Task<String> task = new Task<>(myCallable);
        Thread thread1 = new Thread(new TaskExecutor<>(task));
        Thread thread2 = new Thread(new TaskExecutor<>(task));
        Thread thread3 = new Thread(new TaskExecutor<>(task));

        thread1.start();
        thread2.start();
        Thread.sleep(500);
        thread3.start();
    }
}
