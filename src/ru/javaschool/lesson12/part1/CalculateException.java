package ru.javaschool.lesson12.part1;

public class CalculateException extends RuntimeException {
    public CalculateException() {
    }

    public CalculateException(String message) {
        super(message);
    }

    public CalculateException(String message, Throwable cause) {
        super(message, cause);
    }

    public CalculateException(Throwable cause) {
        super(cause);
    }
}
