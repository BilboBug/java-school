package ru.javaschool.lesson11_1;

import ru.javaschool.lesson11_1.threads.FirstThread;
import ru.javaschool.lesson11_1.threads.QueueThread;
import ru.javaschool.lesson11_1.threads.SecondThread;
import ru.javaschool.lesson11_1.threads.ThirdThread;

public class Main {
    public static void main(String[] args) {

        Foo foo = new Foo();

        QueueThread pool = new QueueThread();

        pool.third(new ThirdThread(foo));
        pool.second(new SecondThread(foo));
        pool.first(new FirstThread(foo));

    }
}
