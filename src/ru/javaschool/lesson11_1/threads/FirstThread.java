package ru.javaschool.lesson11_1.threads;

import ru.javaschool.lesson11_1.Foo;

public class FirstThread implements Runnable {

    Foo foo;

    public FirstThread(Foo foo) {
        this.foo = foo;
    }

    @Override
    public void run() {
        foo.first();
    }
}
