package ru.javaschool.lesson11_1.threads;

import ru.javaschool.lesson11_1.Foo;

public class ThirdThread implements Runnable {

    Foo foo;

    public ThirdThread(Foo foo) {
        this.foo = foo;
    }

    @Override
    public void run() {
        foo.third();
    }
}
