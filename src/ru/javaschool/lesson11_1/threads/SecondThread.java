package ru.javaschool.lesson11_1.threads;

import ru.javaschool.lesson11_1.Foo;

public class SecondThread implements Runnable {

    Foo foo;

    public SecondThread(Foo foo) {
        this.foo = foo;
    }

    @Override
    public void run() {
        foo.second();
    }
}
