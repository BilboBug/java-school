package ru.javaschool.lesson11_1.threads;

public class QueueThread {

    Thread first;
    Thread second;
    Thread third;

    public QueueThread() {
    }

    public void first(Runnable firstThread) {
        Thread thread = new Thread(() -> {
            first = new Thread(firstThread);
            first.start();
        });
        thread.start();
    }

    public void second(Runnable secondThread) {
        Thread thread = new Thread(() -> {
            try {
                while (first == null) {
                    Thread.sleep(50);
                }
                first.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            second = new Thread(secondThread);
            second.start();
        });
        thread.start();
    }

    public void third(Runnable thirdThread) {
        Thread thread = new Thread(() -> {
            try {
                while (second == null || first == null) {
                    Thread.sleep(50);
                }
                first.join();
                second.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            third = new Thread(thirdThread);
            third.start();
        });
        thread.start();
    }
}
