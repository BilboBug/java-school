package ru.javaschool.lesson2;

import java.util.Objects;

public class Credit {
    private static int creditCount = 0;
    private int id;
    private int loanAmount;

    public Credit(int loanAmount) {
        id = creditCount;
        this.loanAmount = loanAmount;
        creditCount++;
    }

    public int getId() {
        return id;
    }

    public int getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(int loanAmount) {
        this.loanAmount = loanAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return id == credit.id && loanAmount == credit.loanAmount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, loanAmount);
    }

    @Override
    public String toString() {
        return "{\n" +
                "\"id\": " + id + ",\n" +
                "\"loanAmount\": " + loanAmount + "\n" +
                '}';
    }
}
