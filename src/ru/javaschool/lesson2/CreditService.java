package ru.javaschool.lesson2;

public interface CreditService {
    public void getLoan(Client client, int loanAmount);
    public void payLoan(Client client, int loanPayment);
}
