package ru.javaschool.lesson2;

public class Test {
    public static void main(String[] args) {
        Client client = new Client("Bogdan", "Treskov");
        Credit credit = new Credit(1000);
        client.setCredit(credit);
        System.out.println(client);
        Client client1 = new Client("Gandalf", "White");
        new CreditServiceImpl().getLoan(client1, 2000);
        System.out.println(client1);
        new CreditServiceImpl().payLoan(client1, 500);
        System.out.println(client1);
        System.out.println(new Transfer(client, client1, 500));
    }
}
