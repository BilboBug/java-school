package ru.javaschool.lesson2;

public class CreditServiceImpl implements  CreditService{
    @Override
    public void getLoan(Client client, int loanAmount) {
        if(client.getCredit() == null)
            client.setCredit(new Credit(loanAmount));
    }

    @Override
    public void payLoan(Client client, int loanPayment) {
        if(client.getCredit() != null) {
            Credit credit = client.getCredit();
            int amount = credit.getLoanAmount();
            if (loanPayment <= amount)
                credit.setLoanAmount(amount - loanPayment);
        }
    }
}
