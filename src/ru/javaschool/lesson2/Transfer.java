package ru.javaschool.lesson2;

import java.util.Objects;

public class Transfer {
    private static int countTransfer = 0;
    private int transferId;
    private Client clientFrom;
    private Client clientTo;
    private int sum;

    public Transfer(Client clientFrom, Client clientTo, int sum) {
        transferId = countTransfer;
        this.clientFrom = clientFrom;
        this.clientTo = clientTo;
        this.sum = sum;
        countTransfer++;
    }

    public int getTransferId() {
        return transferId;
    }

    public void setTransferId(int transferId) {
        this.transferId = transferId;
    }

    public Client getClientFrom() {
        return clientFrom;
    }

    public void setClientFrom(Client clientFrom) {
        this.clientFrom = clientFrom;
    }

    public Client getClientTo() {
        return clientTo;
    }

    public void setClientTo(Client clientTo) {
        this.clientTo = clientTo;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return transferId == transfer.transferId && sum == transfer.sum && Objects.equals(clientFrom, transfer.clientFrom) && Objects.equals(clientTo, transfer.clientTo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transferId, clientFrom, clientTo, sum);
    }

    @Override
    public String toString() {
        return "{\n" +
                "\"transferId\": " + transferId + ",\n" +
                "\"clientFrom\": " + clientFrom + ",\n" +
                "\"clientTo\": " + clientTo + ",\n" +
                "\"sum\": " + sum + "\n" +
                '}';
    }
}
