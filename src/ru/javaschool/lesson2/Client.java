package ru.javaschool.lesson2;

import java.util.Objects;

public class Client {
    private static int clientCount = 0;
    private int id;
    private String firstName;
    private String lastName;
    private Credit credit;

    public Client(String firstName, String lastName) {
        this.id = clientCount;
        this.firstName = firstName;
        this.lastName = lastName;
        credit = null;
        clientCount++;
    }

    public Client(String firstName, String lastName, Credit credit) {
        this.id = clientCount;
        this.firstName = firstName;
        this.lastName = lastName;
        this.credit = credit;
        clientCount++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Credit getCredit() {
        return credit;
    }

    public void setCredit(Credit credit) {
        this.credit = credit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id && firstName.equals(client.firstName) && lastName.equals(client.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName);
    }

    @Override
    public String toString() {
        return "{\n" +
                "\"id\": " + id + ",\n" +
                "\"firstName\": \"" + firstName + "\",\n" +
                "\"lastName\": " + lastName + "\",\n" +
                "\"credit\": " + credit + "\n" +
                "}";
    }
}
